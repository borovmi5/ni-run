#include "ast.d/nodes/access_field.h"

namespace ast {
	AccessField::AccessField(ast_p object, identifier_p field) :
		_object(object), _field(field) {}

	ast_p AccessField::getObject() const { return _object; }
	identifier_p AccessField::getField() const { return _field; }
}
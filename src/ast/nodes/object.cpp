#include "ast.d/nodes/object.h"

namespace ast {
	Object::Object(ast_p extends, std::vector<ast_p> members) :
		_extends(extends), _members(members) {}

	ast_p Object::getExtends() const { return _extends; }
	const std::vector<ast_p>& Object::getMembers() const { return _members; }
}
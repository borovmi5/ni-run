#include "ast.d/nodes/call_method.h"

namespace ast {
	CallMethod::CallMethod(ast_p object, identifier_p name, std::vector<ast_p> arguments) :
		_object(object), _name(name), _arguments(arguments) {}

	ast_p CallMethod::getObject() const { return _object; }
	identifier_p CallMethod::getName() const { return _name; }
	const std::vector<ast_p>& CallMethod::getArguments() const { return _arguments; }
}
#include "ast.d/nodes/boolean.h"

namespace ast {
	Boolean::Boolean(bool value) :
		_value(value) {}

	bool Boolean::getValue() const { return _value; }
}
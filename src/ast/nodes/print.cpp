#include "ast.d/nodes/print.h"

namespace ast {
	Print::Print(std::string format, std::vector<ast_p> arguments) :
		_format(std::move(format)), _arguments(arguments) {}

	const std::string& Print::getFormat() const { return _format; }
	const std::vector<ast_p>& Print::getArguments() const { return _arguments; }
}
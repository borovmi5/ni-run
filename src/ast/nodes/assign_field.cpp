#include "ast.d/nodes/assign_field.h"

namespace ast {
	AssignField::AssignField(ast_p object, identifier_p field, ast_p value) :
		_object(object), _field(field), _value(value) {}

	ast_p AssignField::getObject() const { return _object; }
	identifier_p AssignField::getField() const { return _field; }
	ast_p AssignField::getValue() const { return _value; }
}
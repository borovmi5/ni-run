#include "ast.d/nodes/top.h"

namespace ast {
	Top::Top(std::vector<ast_p> sequence) :
		_sequence(sequence) {}

	const std::vector<ast_p>& Top::getSequence() const { return _sequence; }
}
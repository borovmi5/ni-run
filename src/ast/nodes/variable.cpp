#include "ast.d/nodes/variable.h"

namespace ast {
	Variable::Variable(identifier_p name, ast_p value) :
		_name(name), _value(value) {}

	identifier_p Variable::getName() const { return _name; }
	ast_p Variable::getValue() const { return _value; }
}
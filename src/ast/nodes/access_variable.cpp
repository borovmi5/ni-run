#include "ast.d/nodes/access_variable.h"

namespace ast {
	AccessVariable::AccessVariable(identifier_p name) :
		_name(name) {}

	identifier_p AccessVariable::getName() const { return _name; }
}
#include "ast.d/nodes/loop.h"

namespace ast {
	Loop::Loop(ast_p condition, ast_p body) :
		_condition(condition), _body(body) {}

	ast_p Loop::getCondition() const { return _condition; }
	ast_p Loop::getBody() const { return _body; }
}
#include "ast.d/nodes/integer.h"

namespace ast {
	Integer::Integer(int value) :
		_value(value) {}

	int Integer::getValue() const { return _value; }
}
#include "ast.d/nodes/identifier.h"

namespace ast {
	Identifier::Identifier(std::string name) :
		_name(std::move(name)) {}

	const std::string& Identifier::getName() const { return _name; }
}
#include "ast.d/nodes/call_function.h"

namespace ast {
	CallFunction::CallFunction(identifier_p name, std::vector<ast_p> arguments) :
		_name(name), _arguments(arguments) {}

	identifier_p CallFunction::getName() const { return _name; }
	const std::vector<ast_p>& CallFunction::getArguments() const { return _arguments; }
}
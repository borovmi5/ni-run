#include "ast.d/nodes/array.h"

namespace ast {
	Array::Array(ast_p size, ast_p value) :
		_size(size), _value(value) {}

	ast_p Array::getSize() const { return _size; }
	ast_p Array::getValue() const { return _value; }
}
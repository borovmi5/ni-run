#include "ast.d/nodes/function.h"

namespace ast {
	Function::Function(identifier_p name, std::vector<identifier_p> parameters, ast_p body) :
		_name(name), _parameters(parameters), _body(body) {}

	identifier_p Function::getName() const { return _name; }
	const std::vector<identifier_p>& Function::getParameters() const { return _parameters; }
	ast_p Function::getBody() const { return _body; }
}
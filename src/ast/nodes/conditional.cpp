#include "ast.d/nodes/conditional.h"

namespace ast {
	Conditional::Conditional(ast_p condition, ast_p consequent, ast_p alternative) :
		_condition(condition), _consequent(consequent), _alternative(alternative) {}

	ast_p Conditional::getCondition() const { return _condition; }
	ast_p Conditional::getConsequent() const { return _consequent; }
	ast_p Conditional::getAlternative() const { return _alternative; }
}
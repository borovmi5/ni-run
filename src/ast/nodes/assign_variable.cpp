#include "ast.d/nodes/assign_variable.h"

namespace ast {
	AssignVariable::AssignVariable(identifier_p name, ast_p value) :
		_name(name), _value(value) {}

	identifier_p AssignVariable::getName() const { return _name; }
	ast_p AssignVariable::getValue() const { return _value; }
}
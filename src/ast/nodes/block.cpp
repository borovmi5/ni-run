#include "ast.d/nodes/block.h"

namespace ast {
	Block::Block(std::vector<ast_p> sequence) :
		_sequence(sequence) {}

	const std::vector<ast_p>& Block::getSequence() const { return _sequence; }
}
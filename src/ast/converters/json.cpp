#include <ast.d/converters/json.h>

#include <vector>

namespace ast {
	ast_p jsonToAccessArray(const nlohmann::json& source);
	ast_p jsonToAccessField(const nlohmann::json& source);
	ast_p jsonToAccessVariable(const nlohmann::json& source);
	ast_p jsonToArray(const nlohmann::json& source);
	ast_p jsonToAssignArray(const nlohmann::json& source);
	ast_p jsonToAssignField(const nlohmann::json& source);
	ast_p jsonToAssignVariable(const nlohmann::json& source);
	ast_p jsonToBlock(const nlohmann::json& source);
	ast_p jsonToBoolean(const nlohmann::json& source);
	ast_p jsonToCallFunction(const nlohmann::json& source);
	ast_p jsonToCallMethod(const nlohmann::json& source);
	ast_p jsonToConditional(const nlohmann::json& source);
	ast_p jsonToFunction(const nlohmann::json& source);
	identifier_p jsonToIdentifier(const nlohmann::json& source);
	ast_p jsonToInteger(const nlohmann::json& source);
	ast_p jsonToLoop(const nlohmann::json& source);
	ast_p jsonToNull(const nlohmann::json& source);
	ast_p jsonToObject(const nlohmann::json& source);
	ast_p jsonToPrint(const nlohmann::json& source);
	ast_p jsonToTop(const nlohmann::json& source);
	ast_p jsonToVariable(const nlohmann::json& source);

	ast_p fromJSON(const nlohmann::json& source) {
		if(source.is_string() && source.get<std::string>() == "Null") return jsonToNull(source);
		const auto& node = source.begin();
		auto type = node.key();
		if(type == "AccessArray") return jsonToAccessArray(node.value());
		if(type == "AccessField") return jsonToAccessField(node.value());
		if(type == "AccessVariable") return jsonToAccessVariable(node.value());
		if(type == "Array") return jsonToArray(node.value());
		if(type == "AssignArray") return jsonToAssignArray(node.value());
		if(type == "AssignField") return jsonToAssignField(node.value());
		if(type == "AssignVariable") return jsonToAssignVariable(node.value());
		if(type == "Block") return jsonToBlock(node.value());
		if(type == "Boolean") return jsonToBoolean(node.value());
		if(type == "CallFunction") return jsonToCallFunction(node.value());
		if(type == "CallMethod") return jsonToCallMethod(node.value());
		if(type == "Conditional") return jsonToConditional(node.value());
		if(type == "Function") return jsonToFunction(node.value());
		if(type == "Identifier") return jsonToIdentifier(node.value());
		if(type == "Integer") return jsonToInteger(node.value());
		if(type == "Loop") return jsonToLoop(node.value());
		if(type == "Object") return jsonToObject(node.value());
		if(type == "Print") return jsonToPrint(node.value());
		if(type == "Top") return jsonToTop(node.value());
		if(type == "Variable") return jsonToVariable(node.value());
		throw std::runtime_error("Feature not yet implemented.");
	}

	ast_p fromJSONStream(std::istream& stream) {
		nlohmann::json j;
		stream >> j;
		return fromJSON(j);
	}

	ast_p jsonToAccessArray(const nlohmann::json& source) {
		std::vector<ast_p> arguments;
		arguments.push_back(fromJSON(source["index"]));
		return std::make_shared<CallMethod>(fromJSON(source["array"]), std::make_shared<Identifier>("get"), std::move(arguments));
	}

	ast_p jsonToAccessField(const nlohmann::json& source) {
		return std::make_shared<AccessField>(fromJSON(source["object"]), jsonToIdentifier(source["field"]));
	}

	ast_p jsonToAccessVariable(const nlohmann::json& source) {
		return std::make_shared<AccessVariable>(jsonToIdentifier(source["name"]));
	}

	ast_p jsonToArray(const nlohmann::json& source) {
		return std::make_shared<Array>(fromJSON(source["size"]), fromJSON(source["value"]));
	}

	ast_p jsonToAssignArray(const nlohmann::json& source) {
		std::vector<ast_p> arguments;
		arguments.push_back(fromJSON(source["index"]));
		arguments.push_back(fromJSON(source["value"]));
		return std::make_shared<CallMethod>(fromJSON(source["array"]), std::make_shared<Identifier>("set"), std::move(arguments));
	}

	ast_p jsonToAssignField(const nlohmann::json& source) {
		return std::make_shared<AssignField>(fromJSON(source["object"]), jsonToIdentifier(source["field"]), fromJSON(source["value"]));
	}

	ast_p jsonToAssignVariable(const nlohmann::json& source) {
		return std::make_shared<AssignVariable>(jsonToIdentifier(source["name"]), fromJSON(source["value"]));
	}

	ast_p jsonToBlock(const nlohmann::json& source) {
		std::vector<ast_p> sequence;
		for (const auto & i : source)
			sequence.emplace_back(fromJSON(i));
		return std::make_shared<Block>(std::move(sequence));
	}

	ast_p jsonToBoolean(const nlohmann::json& source) {
		return std::make_shared<Boolean>(source.get<bool>());
	}

	ast_p jsonToCallFunction(const nlohmann::json& source) {
		std::vector<ast_p> arguments;
		for (const auto & i : source["arguments"])
			arguments.emplace_back(fromJSON(i));
		return std::make_shared<CallFunction>(jsonToIdentifier(source["name"]), std::move(arguments));
	}
	ast_p jsonToCallMethod(const nlohmann::json& source) {
		std::vector<ast_p> arguments;
		for (const auto & i : source["arguments"])
			arguments.emplace_back(fromJSON(i));
		return std::make_shared<CallMethod>(fromJSON(source["object"]), jsonToIdentifier(source["name"]), std::move(arguments));

	}

	ast_p jsonToConditional(const nlohmann::json& source) {
		return std::make_shared<Conditional>(fromJSON(source["condition"]), fromJSON(source["consequent"]), fromJSON(source["alternative"]));
	}

	ast_p jsonToFunction(const nlohmann::json& source) {
		std::vector<identifier_p> parameters;
		for (auto i = 0ull; i < source["parameters"].size(); ++i)
			parameters.emplace_back(jsonToIdentifier(source["parameters"].at(i)));
		return std::make_shared<Function>(jsonToIdentifier(source["name"]), std::move(parameters), fromJSON(source["body"]));
	}

	identifier_p jsonToIdentifier(const nlohmann::json& source) {
		return std::make_shared<Identifier>(source.get<std::string>());
	}

	ast_p jsonToInteger(const nlohmann::json& source) {
		return std::make_shared<Integer>(source.get<int>());
	}

	ast_p jsonToLoop(const nlohmann::json& source) {
		return std::make_shared<Loop>(fromJSON(source["condition"]), fromJSON(source["body"]));
	}

	ast_p jsonToNull(const nlohmann::json&) {
		return std::make_shared<Null>();
	}

	ast_p jsonToObject(const nlohmann::json& source) {
		std::vector<ast_p> members;
		for (const auto & i : source["members"])
			members.emplace_back(fromJSON(i));
		return std::make_shared<Object>(fromJSON(source["extends"]), std::move(members));
	}

	ast_p jsonToPrint(const nlohmann::json& source) {
		std::vector<ast_p> arguments;
		for (const auto & i : source["arguments"])
			arguments.emplace_back(fromJSON(i));
		return std::make_shared<Print>(source["format"].get<std::string>(), std::move(arguments));
	}

	ast_p jsonToTop(const nlohmann::json& source) {
		std::vector<ast_p> sequence;
		for (const auto & i : source)
			sequence.emplace_back(fromJSON(i));
		return std::make_shared<Top>(std::move(sequence));
	}

	ast_p jsonToVariable(const nlohmann::json& source) {
		return std::make_shared<Variable>(jsonToIdentifier(source["name"]), fromJSON(source["value"]));
	}
}
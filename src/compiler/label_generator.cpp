#include <compiler.d/label_generator.h>

namespace compiler::labels {
	namespace {
		unsigned long long int next() {
			static unsigned long long int id = 0;
			return id++;
		}
	}

	ConditionalLabels conditional() {
		auto id = next();
		return {
			label("if:consequent:") + std::to_string(id),
			label("if:end:") + std::to_string(id)
		};
	}

	LoopLabels loop() {
		auto id = next();
		return {
			label("loop:body:") + std::to_string(id),
			label("loop:condition:") + std::to_string(id)
		};
	}
}
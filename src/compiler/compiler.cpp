#include <compiler.d/compiler.h>
#include <compiler.d/label_generator.h>

#define NOT_IMPLEMENTED throw std::runtime_error("not implemented");

namespace compiler {
	bc::cpIndex Compiler::getOrCreateConstant(const bc::ProgramObject& object) {
		return _constants.emplace(object, _constants.size()).first->second;
	}

	std::pair<uint16_t, bool> Compiler::getIdent(const std::string& name) {
		if (!_locals.empty()) {
			auto l = _locals.top().find(name);
			if (l != _locals.top().end()) return {l->second, false };
		}
		return { getOrCreateGlobal(name), true };
	}

	bc::cpIndex Compiler::getOrCreateGlobal(const std::string& g) {
		auto id = getOrCreateConstant(g);
		_globals.emplace(g, id);
		return id;
	}

	bc::lfIndex Compiler::createLocal(const std::string& l) {
		auto r = _locals.top().emplace(l, _locals.top().size());
		if(!r.second)
			throw semantic_exception("local ", l, " already declared");
		return r.first->second;
	}

	void Compiler::pushInst(bc::OpCode&& op) {
		_codeStack.top().emplace_back(op);
	}

	void Compiler::popInst() {
		if (!_codeStack.top().empty())
			_codeStack.top().pop_back();
	}

	std::any Compiler::visit(ast::AccessField& node) {
		node.getObject()->accept(*this);
		auto id = getOrCreateConstant(node.getField()->getName());
		pushInst(bc::GetField { id });
		return 0;
	}

	std::any Compiler::visit(ast::AccessVariable& node) {
		auto r = getIdent(node.getName()->getName());
		if (r.second)
			pushInst(bc::GetGlobal { r.first });
		else
			pushInst(bc::GetLocal { r.first });
		return 0;
	}

	std::any Compiler::visit(ast::Array& node) {
		node.getSize()->accept(*this);
		node.getValue()->accept(*this);
		pushInst(bc::Array{});
		return 0;
	}

	std::any Compiler::visit(ast::AssignVariable& node) {
		auto r = getIdent(node.getName()->getName());
		node.getValue()->accept(*this);
		if (r.second)
			pushInst(bc::SetGlobal { r.first });
		else
			pushInst(bc::SetLocal { r.first });
		return 0;
	}

	std::any Compiler::visit(ast::AssignField& node) {
		node.getObject()->accept(*this);
		node.getValue()->accept(*this);
		auto id = getOrCreateConstant(node.getField()->getName());
		pushInst(bc::SetField { id });
		return 0;
	}

	std::any Compiler::visit(ast::Block& node) {
		for (const auto& i : node.getSequence()) {
			i->accept(*this);
			pushInst(bc::Drop {});
		}
		popInst();
		return 0;
	}

	std::any Compiler::visit(ast::Boolean& node) {
		pushInst(bc::Literal { getOrCreateConstant(bc::Boolean(node.getValue())) });
		return 0;
	}

	std::any Compiler::visit(ast::CallFunction& node) {
		for (const auto& a : node.getArguments())
			a->accept(*this);
		auto id = getOrCreateGlobal(node.getName()->getName());
		pushInst(bc::CallFunction { id, bc::arity(node.getArguments().size()) });
		return 0;
	}

	std::any Compiler::visit(ast::CallMethod& node) {
		node.getObject()->accept(*this);
		for (const auto& a : node.getArguments())
			a->accept(*this);
		auto id = getOrCreateConstant(node.getName()->getName());
		pushInst(bc::CallMethod { id, bc::arity(node.getArguments().size() + 1) });
		return 0;
	}

	std::any Compiler::visit(ast::Conditional& node) {
		auto l = labels::conditional();
		auto cid = getOrCreateConstant(l.consequent);
		auto eid = getOrCreateConstant(l.end);
		node.getCondition()->accept(*this);
		pushInst(bc::Branch { cid });
		node.getAlternative()->accept(*this);
		pushInst(bc::Jump { eid });
		pushInst(bc::Label { cid });
		node.getConsequent()->accept(*this);
		pushInst(bc::Label { eid });
		return 0;
	}

	std::any Compiler::visit(ast::Function& node) {
		const auto& name = node.getName()->getName();
		_locals.emplace();
		_codeStack.emplace();
		auto scp = _scope;

		_scope = Scope(_scope | LOCAL_MASK);

		if (isClass(_scope)) createLocal("this");
		for (const auto& p : node.getParameters())
			createLocal(p->getName());
		node.getBody()->accept(*this);
		pushInst(bc::Return {});

		_scope = scp;

		auto args = node.getParameters().size() + (isClass(_scope) ? 1 : 0);
		auto locals = _locals.top().size() - args;
		_locals.pop();
		auto codeStart = _code.size();
		auto codeLength = _codeStack.top().size();
		_code.insert(_code.end(), _codeStack.top().begin(), _codeStack.top().end());
		_codeStack.pop();

		auto id = getOrCreateGlobal(name);
		auto mid = getOrCreateConstant(bc::Method { id, bc::arity(args), bc::size(locals), bc::length(codeLength), codeStart });
		if (isClass(_scope)) return mid;
		_indices.push_back(mid);
		return 1;
	}

	std::any Compiler::visit(ast::Identifier&) {
		NOT_IMPLEMENTED;
	}

	std::any Compiler::visit(ast::Integer& node) {
		pushInst(bc::Literal { getOrCreateConstant(bc::Integer(node.getValue())) });
		return 0;
	}

	std::any Compiler::visit(ast::Loop& node) {
		auto l = labels::loop();
		auto bid = getOrCreateConstant(l.body);
		auto cid = getOrCreateConstant(l.condition);
		pushInst(bc::Jump { cid });
		pushInst(bc::Label { bid });
		node.getBody()->accept(*this);
		pushInst(bc::Drop {});
		pushInst(bc::Label { cid });
		node.getCondition()->accept(*this);
		pushInst(bc::Branch { bid });
		pushInst(bc::Literal { getOrCreateConstant(bc::Null) });
		return 0;
	}

	std::any Compiler::visit(ast::Null&) {
		pushInst(bc::Literal { getOrCreateConstant(bc::Null) });
		return 0;
	}

	std::any Compiler::visit(ast::Object& node) {
		node.getExtends()->accept(*this);
		bc::Class proto;
		auto scp = _scope;

		_scope = CLASS_GLOBAL;
		for (const auto& m : node.getMembers()) {
			proto.emplace_back(std::any_cast<bc::cpIndex>(m->accept(*this)));
		}
		_scope = scp;

		auto cid = getOrCreateConstant(proto);
		pushInst(bc::Object { cid });
		return 0;
	}

	std::any Compiler::visit(ast::Print& node) {
		for (const auto& a : node.getArguments())
			a->accept(*this);
		auto id = getOrCreateConstant(node.getFormat());
		pushInst(bc::Print { id, bc::arity(node.getArguments().size()) });
		return 0;
	}

	std::any Compiler::visit(ast::Top& node) {
		auto id = getOrCreateConstant("λ:");

		_codeStack.emplace();
		_scope = GLOBAL;

		for (const auto& i : node.getSequence()) {
			if (!std::any_cast<int>(i->accept(*this)))
				pushInst(bc::Drop {});
		}
		popInst();

		bc::arity args = 0;
		bc::size locals = 0;
		bc::length codeLength = _codeStack.top().size();
		bc::codeAddr codeStart = _code.size();

		auto entryPoint = getOrCreateConstant(bc::Method { id, bc::arity(args), bc::size(locals), bc::length(codeLength), codeStart });
		_code.insert(_code.end(), _codeStack.top().begin(), _codeStack.top().end());
		_codeStack.pop();

		bc::Code code = _code; _code.clear();
		std::vector<bc::ProgramObject> cp(_constants.size());
		for (const auto& i : _constants) cp[i.second] = i.first;
		_constants.clear();
		GlobalIndices indices = _indices; _indices.clear();
		_globals.clear();
		while(!_locals.empty())
			_locals.pop();

		return bc::Program(bc::ConstantPool(std::move(cp)), std::move(indices), entryPoint, std::move(code));
	}

	std::any Compiler::visit(ast::Variable& node) {
		node.getValue()->accept(*this);
		if (isGlobal(_scope)) {
			auto id = getOrCreateGlobal(node.getName()->getName());
			auto sid = getOrCreateConstant(bc::Slot(id));
			if (isClass(_scope)) return sid;
			pushInst(bc::SetGlobal { id });
			_indices.push_back(sid);
		} else
			pushInst(bc::SetLocal{ createLocal(node.getName()->getName()) });
		return 0;
	}
}

#undef NOT_IMPLEMENTED
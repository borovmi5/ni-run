#include <bytecode.d/constant_pool.h>

namespace bc {
	ConstantPool::ConstantPool(std::vector<ProgramObject>&& data) : _data(data) {}

	const ProgramObject& ConstantPool::get(cpIndex id) const {
		return _data.at(id);
	}

	std::size_t ConstantPool::size() const {
		return _data.size();
	}

	void ConstantPool::push(ProgramObject object) {
		_data.emplace_back(std::move(object));
	}
}
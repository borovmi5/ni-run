#include <bytecode.d/program.h>

namespace bc {
	namespace {
		OpCode opCodeFromStream(std::istream& stream) {
			byte b;
			stream.read((char*)(&b), sizeof(b));
			switch (opcode(b)) {
				case opcode::LABEL: {
					cpIndex n;
					stream.read((char*)(&n), sizeof(n));
					return Label { n };
				}
				case opcode::LITERAL: {
					cpIndex v;
					stream.read((char*)(&v), sizeof(v));
					return Literal { v };
				}
				case opcode::PRINT: {
					cpIndex f;
					arity a;
					stream.read((char*)(&f), sizeof(f));
					stream.read((char*)(&a), sizeof(a));
					return Print { f, a };
				}
				case opcode::ARRAY: {
					return Array {};
				}
				case opcode::OBJECT: {
					cpIndex p;
					stream.read((char*)(&p), sizeof(p));
					return Object { p };
				}
				case opcode::GET_FIELD: {
					cpIndex n;
					stream.read((char*)(&n), sizeof(n));
					return GetField { n };
				}
				case opcode::SET_FIELD: {
					cpIndex n;
					stream.read((char*)(&n), sizeof(n));
					return SetField { n };
				}
				case opcode::CALL_METHOD: {
					cpIndex n;
					arity a;
					stream.read((char*)(&n), sizeof(n));
					stream.read((char*)(&a), sizeof(a));
					return CallMethod { n, a };
				}
				case opcode::CALL_FUNCTION: {
					cpIndex n;
					arity a;
					stream.read((char*)(&n), sizeof(n));
					stream.read((char*)(&a), sizeof(a));
					return CallFunction { n, a };
				}
				case opcode::SET_LOCAL: {
					lfIndex i;
					stream.read((char*)(&i), sizeof(i));
					return SetLocal { i };
				}
				case opcode::GET_LOCAL: {
					lfIndex i;
					stream.read((char*)(&i), sizeof(i));
					return GetLocal { i };
				}
				case opcode::SET_GLOBAL: {
					cpIndex n;
					stream.read((char*)(&n), sizeof(n));
					return SetGlobal { n };
				}
				case opcode::GET_GLOBAL: {
					cpIndex n;
					stream.read((char*)(&n), sizeof(n));
					return GetGlobal { n };
				}
				case opcode::BRANCH: {
					cpIndex l;
					stream.read((char*)(&l), sizeof(l));
					return Branch { l };
				}
				case opcode::JUMP: {
					cpIndex l;
					stream.read((char*)(&l), sizeof(l));
					return Jump { l };
				}
				case opcode::RETURN: {
					return Return {};
				}
				case opcode::DROP: {
					return Drop {};
				}
				default:
					throw std::runtime_error("should never happen");
			}
		}

		std::ostream& opCodeToStream(std::ostream& stream, OpCode op) {
			byte b = op.index();
			stream.write((char*)(&b), sizeof(b));
			switch (opcode(b)) {
				case opcode::LABEL: {
					auto o = std::get<Label>(op);
					stream.write((char*)(&o.name), sizeof(o.name));
					break;
				}
				case opcode::LITERAL: {
					auto o = std::get<Literal>(op);
					stream.write((char*)(&o.value), sizeof(o.value));
					break;
				}
				case opcode::PRINT: {
					auto o = std::get<Print>(op);
					stream.write((char*)(&o.fmt), sizeof(o.fmt));
					stream.write((char*)(&o.args), sizeof(o.args));
					break;
				}
				case opcode::ARRAY: {
					break;
				}
				case opcode::OBJECT: {
					auto o = std::get<Object>(op);
					stream.write((char*)(&o.proto), sizeof(o.proto));
					break;
				}
				case opcode::GET_FIELD: {
					auto o = std::get<GetField>(op);
					stream.write((char*)(&o.name), sizeof(o.name));
					break;
				}
				case opcode::SET_FIELD: {
					auto o = std::get<SetField>(op);
					stream.write((char*)(&o.name), sizeof(o.name));
					break;
				}
				case opcode::CALL_METHOD: {
					auto o = std::get<CallMethod>(op);
					stream.write((char*)(&o.name), sizeof(o.name));
					stream.write((char*)(&o.args), sizeof(o.args));
					break;
				}
				case opcode::CALL_FUNCTION: {
					auto o = std::get<CallFunction>(op);
					stream.write((char*)(&o.name), sizeof(o.name));
					stream.write((char*)(&o.args), sizeof(o.args));
					break;
				}
				case opcode::SET_LOCAL: {
					auto o = std::get<SetLocal>(op);
					stream.write((char*)(&o.id), sizeof(o.id));
					break;
				}
				case opcode::GET_LOCAL: {
					auto o = std::get<GetLocal>(op);
					stream.write((char*)(&o.id), sizeof(o.id));
					break;
				}
				case opcode::SET_GLOBAL: {
					auto o = std::get<SetGlobal>(op);
					stream.write((char*)(&o.name), sizeof(o.name));
					break;
				}
				case opcode::GET_GLOBAL: {
					auto o = std::get<GetGlobal>(op);
					stream.write((char*)(&o.name), sizeof(o.name));
					break;
				}
				case opcode::BRANCH: {
					auto o = std::get<Branch>(op);
					stream.write((char*)(&o.label), sizeof(o.label));
					break;
				}
				case opcode::JUMP: {
					auto o = std::get<Jump>(op);
					stream.write((char*)(&o.label), sizeof(o.label));
					break;
				}
				case opcode::RETURN: {
					break;
				}
				case opcode::DROP: {
					break;
				}
				default:
					throw std::runtime_error("should never happen");
			}
			return stream;
		}

		ProgramObject programObjectFromStream(std::istream& stream, Code& code) {
			byte b;
			stream.read((char*)(&b), sizeof(b));
			switch (type(b)) {
				case type::INTEGER: {
					Integer i;
					stream.read((char*)(&i), sizeof(i));
					return i;
				}
				case type::UNIT: {
					return Null;
				}
				case type::STRING: {
					std::string str;
					length l;
					stream.read((char*)(&l), sizeof(l));
					while (l--) {
						stream.read((char*)(&b), sizeof(b));
						str.push_back(char(b));
					}
					return str;
				}
				case type::METHOD: {
					Method m;
					stream.read((char*)(&m.name), sizeof(m.name));
					stream.read((char*)(&m.args), sizeof(m.args));
					stream.read((char*)(&m.locals), sizeof(m.locals));
					stream.read((char*)(&m.len), sizeof(m.len));
					m.start = code.size();
					for (codeAddr a = 0ull; a < m.len; ++a)
							code.emplace_back(opCodeFromStream(stream));
					return m;
				}
				case type::SLOT: {
					Slot s;
					stream.read((char*)(&s), sizeof(s));
					return s;
				}
				case type::CLASS: {
					size s;
					stream.read((char*)(&s), sizeof(s));
					Class c;
					cpIndex i;
					while (s--) {
						stream.read((char*)(&i), sizeof(i));
						c.emplace_back(i);
					}
					return c;
				}
				case type::BOOLEAN: {
					Boolean v;
					stream.read((char*)(&v), sizeof(v));
					return v;
				}
				default:
					throw std::runtime_error("should never happen");
			}
		}

		std::ostream& programObjectToStream(std::ostream& stream, const ProgramObject& obj, const Code& code) {
			byte b = obj.index();
			stream.write((char*)(&b), sizeof(b));
			switch (type(b)) {
				case type::INTEGER: {
					auto i = std::get<Integer>(obj);
					stream.write((char*)(&i), sizeof(i));
					break;
				}
				case type::UNIT: {
					break;
				}
				case type::STRING: {
					auto str = std::get<String>(obj);
					length l = str.length();
					stream.write((char*)(&l), sizeof(l));
					stream.write(str.data(), l * sizeof(char));
					break;
				}
				case type::METHOD: {
					auto m = std::get<Method>(obj);
					stream.write((char*)(&m.name), sizeof(m.name));
					stream.write((char*)(&m.args), sizeof(m.args));
					stream.write((char*)(&m.locals), sizeof(m.locals));
					stream.write((char*)(&m.len), sizeof(m.len));
					for (codeAddr a = m.start; a < m.start + m.len; ++a)
						opCodeToStream(stream, code[a]);
					break;
				}
				case type::SLOT: {
					Slot s = std::get<Slot>(obj);
					stream.write((char*)(&s), sizeof(s));
					break;
				}
				case type::CLASS: {
					auto c = std::get<Class>(obj);
					size s = c.size();
					stream.write((char*)(&s), sizeof(s));
					stream.write((char*)c.data(), s * sizeof(cpIndex));
					break;
				}
				case type::BOOLEAN: {
					Boolean v = std::get<Boolean>(obj);
					stream.write((char*)(&v), sizeof(v));
					break;
				}
				default:
					throw std::runtime_error("should never happen");
			}
			return stream;
		}
	}

	Program::Program(ConstantPool&& pool, Globals&& globals, cpIndex ep, Code&& code) :
	_constPool(pool), _globals(globals), _entryPoint(ep), _code(code) {}

	const ProgramObject& Program::getConst(cpIndex id) const {
		return _constPool.get(id);
	}

	std::size_t Program::constCount() const {
		return _constPool.size();
	}

	const ProgramObject& Program::getGlobal(cpIndex id) const {
		return _constPool.get(_globals.at(id));
	}

	cpIndex Program::getGlobalIndex(cpIndex id) const {
		return _globals.at(id);
	}

	std::size_t Program::globalCount() const {
		return _globals.size();
	}

	codeAddr Program::getEntryPoint() const {
		return std::get<Method>(_constPool.get(_entryPoint)).start;
	}

	arity Program::getArity() const {
		return std::get<Method>(_constPool.get(_entryPoint)).args;
	}

	size Program::getLocals() const {
		return std::get<Method>(_constPool.get(_entryPoint)).locals;
	}

	const OpCode& Program::getCode(codeAddr addr) const {
		return _code.at(addr);
	}

	Program Program::fromStream(std::istream& stream) {
		Program p;
		// Const pool
		size cpSize;
		stream.read((char*)(&cpSize), sizeof(cpSize));
		while (cpSize--)
			p._constPool.push(programObjectFromStream(stream, p._code));
		// Globals
		size globSize;
		stream.read((char*)(&globSize), sizeof(globSize));
		cpIndex i;
		while (globSize--) {
			stream.read((char*)(&i), sizeof(i));
			p._globals.emplace_back(i);
		}
		// Entry point
		stream.read((char*)(&p._entryPoint), sizeof(p._entryPoint));
		return p;
	}

	std::ostream& Program::toStream(std::ostream& stream) const {
		size cpSize = _constPool.size();
		stream.write((char*)&cpSize, sizeof(cpSize));
		for (cpIndex i = 0; i < cpSize; ++i)
			programObjectToStream(stream, _constPool.get(i), _code);
		size globSize = _globals.size();
		stream.write((char*)&globSize, sizeof(globSize));
		for (auto i : _globals)
			stream.write((char*)&i, sizeof(i));
		stream.write((char*)&_entryPoint, sizeof(_entryPoint));
		return stream;
	}

	codeAddr Program::getEndPoint() const {
		return _code.size();
	}
}
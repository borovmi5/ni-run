#include <ast>

#include <bytecode>
#include <compiler>
#include <interpreters>

#include <cstring>
#include <fstream>
#include <iostream>

#include <argp.h>

using namespace std;

const char *argp_program_version = "fml 0.5.0.0";
static char doc[] = "FML VM";
static char args_doc[] = "ACTION FILE";

static struct argp_option options[] = {
	{"output", 'o', "FILE", 0, "output file", 0},
	{"heap-size", 'H', "SIZE", 0, "size of heap in MiB (or in specified unit: B, KiB, MiB, GiB, TiB", 0},
	{"heap-log", 'l', "FILE", 0, "heap log file", 0},
	{"no-gc", 'G', nullptr, 0, "run without GC", 0},
	{nullptr, 0, nullptr, 0, nullptr, 0}
};

#define B * 1
#define kiB B * 1024
#define MiB kiB * 1024
#define GiB MiB * 1024
#define TiB GiB * 1024

struct args {
	std::ifstream input;
	bool out = false;
	std::ofstream output;

	std::size_t heapSize = 128 MiB;
	bool log = false;
	std::ofstream logFile;

	bool noGC = false;

	enum { RUN, COMPILE, EXECUTE } action = EXECUTE;
};

static error_t parse_opt(int key, char *arg, argp_state *state) {
	args *args = (struct args*)(state->input);
	switch (key) {
		case 'o': {
			args->out = true;
			args->output.open(arg, ios::binary);
			if (!args->output) argp_usage(state);
			break;
		}
		case 'H': {
			char * end;
			std::size_t val = strtoull(arg, &end, 10);
			if (args->heapSize < 1)
				argp_error(state, "heap size must be a positive integer");
			if (!strcmp(end, "B")) args->heapSize = val B;
			else if (!strcmp(end, "kiB")) args->heapSize = val kiB;
			else if (!strcmp(end, "MiB") || !*end) args->heapSize = val MiB;
			else if (!strcmp(end, "GiB")) args->heapSize = val GiB;
			else if (!strcmp(end, "TiB")) args->heapSize = val TiB;
			else argp_error(state, "heap size must be a positive integer");
			break;
		}
		case 'l': {
			args->log = true;
			args->logFile.open(arg);
			if (!args->logFile) argp_usage(state);
			break;
		}
		case 'G': {
			args->noGC = true;
			break;
		}
		case ARGP_KEY_ARG: {
			switch (state->arg_num) {
				case 0:
					if (!strcmp(arg, "run")) args->action = args::RUN;
					else if (!strcmp(arg, "compile")) args->action = args::COMPILE;
					else if (!strcmp(arg, "execute")) args->action = args::EXECUTE;
					else argp_usage(state);
					break;
				case 1:
					args->input.open(arg, ios::binary);
					if(!args->input) argp_usage(state);
					break;
				default: argp_usage(state);
			}
			break;
		}
		case ARGP_KEY_END: {
			if (state->arg_num < 2) argp_usage(state);
			if (args->action != args::COMPILE && args->out)
				argp_error(state, "output file can be selected only for compile action");
			if (args->action == args::COMPILE && !args->out)
				argp_error(state, "output file must be selected for compile action");
			break;
		}
		default: {
			return ARGP_ERR_UNKNOWN;
		}
	}
	return 0;
}

static struct argp argp = { options, parse_opt, args_doc, doc, nullptr, nullptr, nullptr};

#define DECL_INTERPRETER(name, args) interpreters::BCInterpreter (name)((args).heapSize, args.noGC ? interpreters::GCType::NO_GC : interpreters::GCType::MARK_AND_SWEEP, (args).log ? interpreters::CsvReporter::make((args).logFile) : interpreters::VoidReporter::make())

int main(int argc, char ** argv) {
	struct args args;
	argp_parse(&argp, argc, argv, 0, nullptr, &args);

	try {
		switch (args.action) {
			case args::RUN: {
				auto ast = ast::fromJSONStream(args.input);
				compiler::Compiler c;
				auto program = std::any_cast<bc::Program>(ast->accept(c));
				DECL_INTERPRETER(interpreter, args);
				interpreter(program);
				break;
			}
			case args::COMPILE: {
				auto ast = ast::fromJSONStream(args.input);
				compiler::Compiler c;
				auto res = std::any_cast<bc::Program>(ast->accept(c));
				res.toStream(args.output);
				break;
			}
			case args::EXECUTE: {
				auto program = bc::Program::fromStream(args.input);
				DECL_INTERPRETER(interpreter, args);
				interpreter(program);
				break;
			}
		}
	} catch(compiler::semantic_exception& e) {
		cerr << "Semantic error: " << e.what() << endl;
	} catch(interpreters::interpret_exception& e) {
		cerr << "Interpretation error: " << e.what() << endl;
		return 1;
	} catch(interpreters::critical_error& e) {
		cerr << "Critical error: " << e.what() << endl;
		return 2;
	} catch(exception& e) {
		cerr << "Error: " << e.what() << endl;
		return 3;
	}

	return 0;
}
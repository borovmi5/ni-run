#include <interpreters.d/interpreter.h>

#include <fstream>
#include <iostream>

#define PUSH_NEW(x) _stack.push(_heap.alloc(x))
#define PUSH_NEW2(x,y) _stack.push(_heap.alloc(x, y))
#define STRING(x) std::get<bc::String>(_program.getConst(x))
#define CLASS(x) std::get<bc::Class>(_program.getConst(x))
#define INT(x) std::get<Integer>(x)
#define BOOL(x) std::get<Boolean>(x)
#define OBJ(x) std::get<Object>(x)

namespace interpreters {
	std::string BCInterpreter::print(const bc::ProgramObject& o) {
		switch (bc::type(o.index())) {
			case bc::type::INTEGER:
				return std::to_string(std::get<bc::Integer>(o));
			case bc::type::UNIT:
				return "null";
			case bc::type::STRING:
				return std::get<bc::String>(o);
			case bc::type::METHOD: {
				auto m = std::get<bc::Method>(o);
				std::ostringstream oss;
				oss << "#method(" << m.start << '-' << (m.start + m.len - 1) << ")";
				return oss.str();
			}
			case bc::type::SLOT: {
				std::ostringstream oss;
				oss << "#slot(" << std::get<bc::Slot>(o) << ")";
				return oss.str();
			}
			case bc::type::CLASS: {
				auto c = std::get<bc::Class>(o);
				std::ostringstream oss;
				oss << "#class(";
				for (auto i : c)
					oss << i << ',';
				oss << ")";
				return oss.str();
			}
			case bc::type::BOOLEAN:
				return std::get<bc::Boolean>(o) ? "true" : "false";
			default:
				throw critical_error("should never happen");
		}
	}

	std::string BCInterpreter::print(const RuntimeObject& o) {
		switch (type(o.index())) {
			case type::UNIT:
				return "null";
			case type::INTEGER:
				return std::to_string(std::get<Integer>(o));
			case type::BOOLEAN:
				return std::get<Boolean>(o) ? "true" : "false";
			case type::ARRAY: {
				auto arr = std::get<Array>(o);
				std::ostringstream oss;
				oss << "[";
				for (auto i = 0; i < arr.size - 1; ++i)
					oss << print(deref(arr.content[i])) << ",";
				oss << print(deref(arr.content[arr.size - 1]));
				oss << "]";
				return oss.str();
			}
			case type::OBJECT: {
				auto obj = std::get<Object>(o);
				std::ostringstream oss;
				oss << "object(";
				if (obj.parent != NULL_PTR) {
					oss << "..=" << print(deref(obj.parent));
				}
				auto idx = 0;
				for (auto i : CLASS(obj.proto)) {
					auto v = _program.getConst(i);
					if (std::holds_alternative<bc::Slot>(v)) {
						if (idx || obj.parent != NULL_PTR) oss << ", ";
						oss << STRING(std::get<bc::Slot>(v)) << "=" << print(deref(obj.content[idx++]));
					}
				}
				oss << ")";
				return oss.str();
			}
			default:
				throw critical_error("should never happen");
		}
	}

	namespace {
		/*
		 * TODO: rework, passing so many λs is ugly :(
		 */
		Heap::GCGenerator partialGC(GCType gcType, GlobalFrame& glb, FrameStack& fs, OperandStack& os, const bc::Program& p) {
			switch (gcType) {
				case GCType::NO_GC:
					return [](Pointer){ return NoGarbageCollector::make(); };
				case GCType::MARK_AND_SWEEP:
					auto fieldsCnt = [&p](const Object& obj) {
						auto proto = std::get<bc::Class>(p.getConst(obj.proto));
						auto cnt   = 0;

						for(auto i: proto) {
							const auto& v = p.getConst(i);
							if(std::holds_alternative<bc::Slot>(v))
								++cnt;
						}

						return cnt;
					};
					auto roots = [&glb, &fs, &os]() {
						MarkAndSweep::Queue q;
						for (auto& p : glb)
							q.push(&p.second);
						for (auto& f : fs)
							for (auto& p : f.values)
								q.push(&p);
						for (auto& p : os)
							q.push(&p);
						return q;
					};
					return [fieldsCnt, roots](Pointer data){ return MarkAndSweep::make(data, std::move(fieldsCnt), std::move(roots)); };
			}
			return [](Pointer){ return gc_p(nullptr); };
		}
	}

	BCInterpreter::BCInterpreter(std::size_t heapSize, GCType gcType, heap_reporter_p reporter):
		_heap(heapSize, partialGC(gcType, _globalFrame, _frameStack, _stack, _program), std::move(reporter)) {}

	void BCInterpreter::setProgram(bc::Program program) {
		_labelTable.clear();
		_globalFrame.clear();
		_functions.clear();
		_program = std::move(program);

		for (bc::codeAddr a = 0ull; a < _program.getEndPoint(); ++a) {
			if (std::holds_alternative<bc::Label>(_program.getCode(a))) {
				auto label = _program.getConst(std::get<bc::Label>(_program.getCode(a)).name);
				auto tag = std::get<bc::String>(label);
				auto res = _labelTable.insert({ tag, a });
				if (!res.second)
					throw critical_error("label '", res.first->first, "' multiple definitions");
			}
		}

		for (auto g = 0ull; g < _program.globalCount(); ++g) {
			auto po = _program.getGlobal(g);
			if (std::holds_alternative<bc::Slot>(po)) {
				auto s = std::get<bc::String>(_program.getConst(std::get<bc::Slot>(po)));
				auto r = _globalFrame.insert({ s, _heap.alloc(Null) });
				if (!r.second)
					throw critical_error("global variable '", r.first->first, "' multiple definitions");
			} else if (std::holds_alternative<bc::Method>(po)) {
				auto s = std::get<bc::String>(_program.getConst(std::get<bc::Method>(po).name));
				auto r = _functions.insert({ s, _program.getGlobalIndex(g) });
				if (!r.second)
					throw critical_error("function '", r.first->first, "' multiple definitions");
			} else
				throw critical_error("invalid index in globals");
		}

		this->reset();
	}

	void BCInterpreter::setProgram(std::ifstream& source) {
		this->setProgram(bc::Program::fromStream(source));
	}

	void BCInterpreter::reset() {
		_pc = _program.getEntryPoint();
		_heap.clear();
		while (!_stack.empty())
			_stack.pop();
		while (!_frameStack.empty())
			_frameStack.pop_back();
		_frameStack.emplace_back(Frame{ std::vector<Pointer>(_program.getArity() + _program.getLocals()), _program.getEndPoint() });
		_heap.clear();
		for (auto& g: _globalFrame)
			g.second = _heap.alloc(Null);
	}

	void BCInterpreter::step() {
		auto op = _program.getCode(_pc++);

		switch (bc::opcode(op.index())) {
			case bc::opcode::LABEL:
				process(std::get<bc::Label>(op));
				break;
			case bc::opcode::LITERAL:
				process(std::get<bc::Literal>(op));
				break;
			case bc::opcode::PRINT:
				process(std::get<bc::Print>(op));
				break;
			case bc::opcode::ARRAY:
				process(std::get<bc::Array>(op));
				break;
			case bc::opcode::OBJECT:
				process(std::get<bc::Object>(op));
				break;
			case bc::opcode::GET_FIELD:
				process(std::get<bc::GetField>(op));
				break;
			case bc::opcode::SET_FIELD:
				process(std::get<bc::SetField>(op));
				break;
			case bc::opcode::CALL_METHOD:
				process(std::get<bc::CallMethod>(op));
				break;
			case bc::opcode::CALL_FUNCTION:
				process(std::get<bc::CallFunction>(op));
				break;
			case bc::opcode::SET_LOCAL:
				process(std::get<bc::SetLocal>(op));
				break;
			case bc::opcode::GET_LOCAL:
				process(std::get<bc::GetLocal>(op));
				break;
			case bc::opcode::SET_GLOBAL:
				process(std::get<bc::SetGlobal>(op));
				break;
			case bc::opcode::GET_GLOBAL:
				process(std::get<bc::GetGlobal>(op));
				break;
			case bc::opcode::BRANCH:
				process(std::get<bc::Branch>(op));
				break;
			case bc::opcode::JUMP:
				process(std::get<bc::Jump>(op));
				break;
			case bc::opcode::RETURN:
				process(std::get<bc::Return>(op));
				break;
			case bc::opcode::DROP:
				process(std::get<bc::Drop>(op));
				break;
			default:
				throw critical_error("should never happen");
		}
	}

	void BCInterpreter::run() {
		while (_pc < _program.getEndPoint())
			this->step();
	}

	void BCInterpreter::operator()(bc::Program program) {
		this->setProgram(std::move(program));
		this->run();
	}

	void BCInterpreter::operator()(std::ifstream& source) {
		(*this)(bc::Program::fromStream(source));
	}

	void BCInterpreter::process(bc::Label) {}

	void BCInterpreter::process(bc::Literal op) {
		auto v = _program.getConst(op.value);
		switch (bc::type(v.index())) {
			case bc::type::INTEGER: {
				PUSH_NEW(Integer(std::get<bc::Integer>(v)));
				break;
			}
			case bc::type::UNIT: {
				PUSH_NEW(Null);
				break;
			}
			case bc::type::BOOLEAN: {
				PUSH_NEW(Boolean(std::get<bc::Boolean>(v)));
				break;
			}
			default:
				throw interpret_exception('\'', print(v), "' is not a literal type");
		}
	}

	void BCInterpreter::process(bc::Print op) {
		auto fmt = _program.getConst(op.fmt);
		if (!std::holds_alternative<bc::String>(fmt))
			throw interpret_exception("function 'print' expects format to be #string but got '", print(fmt), "'");
		std::istringstream iss(std::get<bc::String>(fmt));
		std::ostringstream oss;

		char c;
		long long int i, cnt;
		i = cnt = (long long int)(op.args + 1ull);
		auto s = 0ull;
		do {

			s = iss.str().find('~', s) + 1;
			--i;
		} while (s != std::string::npos + 1);
		if (i)
			throw interpret_exception("function 'print' expects ", std::abs(cnt - i + 1ll), " argument(s) but got ", cnt + 1);

		std::stack<std::string> args;
		for (auto arg = 0ull; arg < op.args; ++arg)
			args.push(print(_stack.popRO()));

		while ((c = char(iss.get())) && !iss.eof()) {
			if (c == '~') {
				oss << args.top();
				args.pop();
			} else if (c == '\\') {
				if ((c = char(iss.get())) && !iss.eof()) {
					switch (c) {
						case 'n': oss << '\n'; break;
						case 'r': oss << '\r'; break;
						case 't': oss << '\t'; break;
						case '\\': oss << '\\'; break;
						case '~' : oss << '~'; break;
						case '"' : oss << '"'; break;
						default:
							throw interpret_exception("unknown escape sequence '\\", c, "'");
					}
				} else {
					throw interpret_exception("function 'print' unfinished format string");
				}
			} else {
				oss << c;
			}
		}
		std::cout << oss.str();

		PUSH_NEW(Null);
	}

	void BCInterpreter::process(bc::Array) {
		auto init = _stack.popPtr();
		auto size = _stack.popRO();

		if (!std::holds_alternative<Integer>(size) || std::get<Integer>(size) < 1)
			throw interpret_exception("array expects size to be positive #int but got '", print(size), "'");

		PUSH_NEW2((Array{ std::get<Integer>(size), nullptr }), init);
	}

	void BCInterpreter::process(bc::Object op) {
		auto proto = CLASS(op.proto);
		std::stack<Pointer> fields;

		for (auto i : proto) {
			if (std::holds_alternative<bc::Slot>(_program.getConst(i)))
				fields.push(_stack.popPtr());
		}

		PUSH_NEW2((Object{ op.proto, _stack.popPtr(), nullptr }), std::move(fields));
	}

	void BCInterpreter::process(bc::GetField op) {
		auto fname = STRING(op.name);

		auto receiver = _stack.popRO();
		auto recObj = receiver;

		while (std::holds_alternative<Object>(receiver)) {
			auto obj   = OBJ(receiver);
			auto proto = CLASS(obj.proto);
			auto idx   = 0;

			for(auto i: proto) {
				auto v = _program.getConst(i);
				if(std::holds_alternative<bc::Slot>(v)) {
					if(STRING(std::get<bc::Slot>(v)) == fname) {
						_stack.push(obj.content[idx]);
						return;
					}
					++idx;
				}
			}

			receiver = deref(obj.parent);
		}
		throw interpret_exception("no field '", fname, "' in '", print(receiver), "'");
	}

	void BCInterpreter::process(bc::SetField op) {
		auto fname = STRING(op.name);

		auto value = _stack.popPtr();
		auto receiver = _stack.popRO();

		while (std::holds_alternative<Object>(receiver)) {
			auto obj   = OBJ(receiver);
			auto proto = CLASS(obj.proto);
			auto idx   = 0;

			for(auto i: proto) {
				auto v = _program.getConst(i);
				if(std::holds_alternative<bc::Slot>(v)) {
					if(STRING(std::get<bc::Slot>(v)) == fname) {
						_stack.push(obj.content[idx] = value);
						return;
					}
					++idx;
				}
			}

			receiver = deref(obj.parent);
		}
		throw interpret_exception("no field '", fname, "' in '", print(receiver), "'");
	}

	void BCInterpreter::process(bc::CallMethod op) {
		std::stack<Pointer> args;
		#define NEXT_RO() deref(args.top()); args.pop()
		#define NEXT_PTR() args.top(); args.pop()
		for (auto i = op.args; i; --i) args.push(_stack.popPtr());
		auto recPtr = NEXT_PTR();
		auto receiver = deref(recPtr);
		auto recObj = receiver;

		#define TEST_ARGS(exp) \
			if(exp != op.args) throw interpret_exception("method '", m, "' in '", print(recObj), "' expects ", exp, " argument(s) but got ", op.args)
		#define UNDEFINED() throw interpret_exception("no method '", m, "' in '", print(recObj), "'")
		#define TEST_INT(sub) if(!std::holds_alternative<Integer>(sub)) throw interpret_exception("method '", op.name, "' in '", print(recObj), "' expects its argument to be #int but got '", print(sub), "'")
		#define TEST_BOOL(sub) if(!std::holds_alternative<Boolean>(sub)) throw interpret_exception("method '", op.name, "' in '", print(recObj), "' expects its argument to be #bool but got '", print(sub), "'")

		auto m = STRING(op.name);
		while (true) {
			switch (type(receiver.index())) {
				case type::UNIT: {
					TEST_ARGS(2);
					auto lhs = receiver, rhs = NEXT_RO();
					if (m == "==" || m == "eq") PUSH_NEW(std::holds_alternative<Unit>(rhs));
					else if (m == "!=" || m == "neq") PUSH_NEW(!std::holds_alternative<Unit>(rhs));
					else
						UNDEFINED();
					return;
				}
				case type::INTEGER: {
					TEST_ARGS(2);
					auto lhs = receiver, rhs = NEXT_RO();
					if      (m == "+"  || m == "add") {
						TEST_INT(rhs); PUSH_NEW(INT(lhs) +  INT(rhs)); }
					else if (m == "-"  || m == "sub") {
						TEST_INT(rhs); PUSH_NEW(INT(lhs) -  INT(rhs)); }
					else if (m == "*"  || m == "mul") {
						TEST_INT(rhs); PUSH_NEW(INT(lhs) *  INT(rhs)); }
					else if (m == "/"  || m == "div") {
						TEST_INT(rhs); PUSH_NEW(INT(lhs) /  INT(rhs)); }
					else if (m == "%"  || m == "mod") {
						TEST_INT(rhs); PUSH_NEW(INT(lhs) %  INT(rhs)); }
					else if (m == "<=" || m == "le")  {
						TEST_INT(rhs); PUSH_NEW(INT(lhs) <= INT(rhs)); }
					else if (m == ">=" || m == "ge")  {
						TEST_INT(rhs); PUSH_NEW(INT(lhs) >= INT(rhs)); }
					else if (m == "<"  || m == "lt")  {
						TEST_INT(rhs); PUSH_NEW(INT(lhs) <  INT(rhs)); }
					else if (m == ">"  || m == "gt")  {
						TEST_INT(rhs); PUSH_NEW(INT(lhs) >  INT(rhs)); }
					else if (m == "==" || m == "eq")  { PUSH_NEW(std::holds_alternative<Integer>(rhs) && INT(lhs) == INT(rhs)); }
					else if (m == "!=" || m == "neq") { PUSH_NEW(!std::holds_alternative<Integer>(rhs) || INT(lhs) != INT(rhs)); }
					else
						UNDEFINED();
					return;
				}
				case type::BOOLEAN: {
					TEST_ARGS(2);
					auto lhs = receiver, rhs = NEXT_RO();
					if      (m == "&"  || m == "and") {
						TEST_BOOL(rhs); PUSH_NEW(BOOL(lhs) && BOOL(rhs)); }
					else if (m == "|"  || m == "or")  {
						TEST_BOOL(rhs); PUSH_NEW(BOOL(lhs) || BOOL(rhs)); }
					else if (m == "==" || m == "eq")  { PUSH_NEW(std::holds_alternative<Boolean>(rhs) && BOOL(lhs) == BOOL(rhs)); }
					else if (m == "!=" || m == "neq") { PUSH_NEW(!std::holds_alternative<Boolean>(rhs) || BOOL(lhs) != BOOL(rhs)); }
					else
						UNDEFINED();
					return;
				}
				case type::ARRAY: {
					auto a = std::get<Array>(receiver);
					if (m == "get") {
						TEST_ARGS(2);
						auto idx = NEXT_RO();
						TEST_INT(idx);
						auto i = INT(idx);
						if (i < 0 || i >= a.size)
							throw interpret_exception("method 'get' in '", print(a), "' index ", i, " out of range");
						_stack.push(a.content[i]);
					} else if (m == "set") {
						TEST_ARGS(3);
						auto idx = NEXT_RO();
						auto val = NEXT_PTR();
						TEST_INT(idx);
						auto i = INT(idx);
						if (i < 0 || i >= a.size)
							throw interpret_exception("method 'get' in '", print(a), "' index ", i, " out of range");
						a.content[i] = val;
						_stack.push(val);
					} else
						UNDEFINED();
					return;
				}
				case type::OBJECT: {
					auto obj = OBJ(receiver);
					auto proto = CLASS(obj.proto);

					for (auto i : proto) {
						auto v = _program.getConst(i);
						switch(bc::type(v.index())) {
							case bc::type::METHOD:
								if (m == STRING(std::get<bc::Method>(v).name)) {
									auto f = std::get<bc::Method>(v);
									if (op.args != f.args)
										throw interpret_exception("method '", m, "' in '",
										                          print(receiver), "' expects ", f.args, " argument(s) but got ", op.args);
									_frameStack.emplace_back(Frame{ std::vector<Pointer>(f.args + f.locals), _pc });
									_frameStack.back().values[0] = recPtr;
									for (auto i = 1; i < f.args; ++i) {
										_frameStack.back().values[i] = NEXT_PTR();
									}
									_pc = f.start;
									return;
								}
								break;
							case bc::type::SLOT:
								if (m == STRING(std::get<bc::Slot>(v)))
									throw interpret_exception("field '", m, "' in '",
									                          print(recObj), "' called as a function");
								break;
							default:
								break;
						}
					}

					if (obj.parent == NULL_PTR)
						UNDEFINED();
					receiver = deref(obj.parent);
					break;
				}
				default:
					throw critical_error("should never happen");
			}
		}

		#undef NEXT_PTR
		#undef NEXT_RO
		#undef TEST_INT
		#undef TEST_BOOL
		#undef UNDEFINED
		#undef TEST_ARGS
	}

	void BCInterpreter::process(bc::CallFunction op) {
		auto n = STRING(op.name);
		auto r = _functions.find(n);
		if (r == _functions.end())
			throw interpret_exception("function '", n, "' not defined");
		auto f = std::get<bc::Method>(_program.getConst(r->second));
		if (op.args != f.args)
			throw interpret_exception("function '", n, "' expects ", f.args, " argument(s) but got ", op.args);
		_frameStack.emplace_back(Frame{ std::vector<Pointer>(f.args + f.locals), _pc });
		for (auto i = 0; i < f.args; ++i)
			_frameStack.back().values[f.args - i - 1] = _stack.popPtr();
		_pc = f.start;
	}

	void BCInterpreter::process(bc::SetLocal op) {
		_frameStack.back().values[op.id] = _stack.peekPtr();
	}

	void BCInterpreter::process(bc::GetLocal op) {
		_stack.push(_frameStack.back().values[op.id]);
	}

	void BCInterpreter::process(bc::SetGlobal op) {
		auto n = STRING(op.name);
		auto loc = _globalFrame.find(n);
		if (loc == _globalFrame.end())
			throw interpret_exception("global variable '", n, "' not defined");
		loc->second = _stack.peekPtr();
	}

	void BCInterpreter::process(bc::GetGlobal op) {
		auto n = STRING(op.name);
		auto loc = _globalFrame.find(n);
		if (loc == _globalFrame.end())
			throw interpret_exception("global variable '", n, "' not defined");
		_stack.push(loc->second);
	}

	void BCInterpreter::process(bc::Branch op) {
		auto cond = _stack.popRO();

		if (std::holds_alternative<Unit>(cond) || (std::holds_alternative<Boolean>(cond) && !std::get<Boolean>(cond)))
			return;

		auto label = STRING(op.label);
		auto loc = _labelTable.find(label);
		if (loc == _labelTable.end())
			throw critical_error("label '", label, "' not defined");
		_pc = loc->second;
	}

	void BCInterpreter::process(bc::Jump op) {
		auto label = STRING(op.label);
		auto loc = _labelTable.find(label);
		if (loc == _labelTable.end())
			throw critical_error("label '", label, "' not defined");
		_pc = loc->second;
	}

	void BCInterpreter::process(bc::Return) {
		_pc = _frameStack.back().ra;
		_frameStack.pop_back();
	}

	void BCInterpreter::process(bc::Drop) {
		_stack.pop();
	}
}

#undef BOOL
#undef INT
#undef OBJ
#undef CLASS
#undef STRING
#undef PUSH_NEW2
#undef PUSH_NEW
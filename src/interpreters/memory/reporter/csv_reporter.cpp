#include <interpreters.d/memory/reporter.d/csv_reporter.h>

#include <chrono>

namespace interpreters {
	auto timestamp() {
		return std::chrono::duration_cast<std::chrono::nanoseconds>(std::chrono::system_clock::now().time_since_epoch()).count();
	}

	CsvReporter::CsvReporter(std::ostream& out):
		_out(out) {

		_out << "timestamp,event,heap" << std::endl;
	}

	heap_reporter_p CsvReporter::make(std::ostream& out) {
		return std::make_unique<CsvReporter>(out);
	}

	void CsvReporter::report(HeapEvent e) const {
		_out << timestamp() << ',' << toString(e) << ',' << _size() << std::endl;
	}
}
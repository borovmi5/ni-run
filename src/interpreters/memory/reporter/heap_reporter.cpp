#include <interpreters.d/memory/reporter.d/heap_reporter.h>

namespace interpreters {
	void HeapReporter::setSizeFn(SizeFn size) {
		_size = std::move(size);
	}
}
#include <interpreters.d/memory/reporter.d/void_reporter.h>

namespace interpreters {
	heap_reporter_p VoidReporter::make() {
		return std::make_unique<VoidReporter>();
	}

	void VoidReporter::report(HeapEvent) const {}
}
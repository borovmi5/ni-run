#include <interpreters.d/memory/gc.d/mark_and_sweep.h>
#include <interpreters.d/memory/pointer_tagging.h>

#include <cstring>
#include <utility>

namespace interpreters {
	MarkAndSweep::RefMap MarkAndSweep::mark(Queue q) const {
		RefMap r;
		while (!q.empty()) {
			auto p = q.front(); q.pop();
			auto ro = deref(*p);
			if (!(std::holds_alternative<Array>(ro) || std::holds_alternative<Object>(ro))) continue;
			auto rf = r.insert(std::make_pair(*p, std::make_pair(0, std::vector<Pointer*>())));
			rf.first->second.second.push_back(p);
			if (!rf.second) continue;
			if (std::holds_alternative<Array>(ro)) {
				auto& arr = std::get<Array>(ro);
				rf.first->second.first = sizeof(arr.size) + arr.size * sizeof(Pointer);
				for (auto i = 0; i < arr.size; ++i)
					q.push(arr.content + i);
			} else {
				auto& obj = std::get<Object>(ro);
				auto sz = _fieldCnt(obj);
				rf.first->second.first = sizeof(obj.proto) + sizeof(obj.parent) + sz * sizeof(Pointer);
				for (auto i = 0; i < sz; ++i)
					q.push(obj.content + i);
			}
		}
		return r;
	}

	namespace {
		void align(Pointer& p) {
			p += (TAG_SIZE - (uint64_t(p) & uint64_t(tag::MASK))) % TAG_SIZE;
		}
	}

	std::pair<MarkAndSweep::PosMap, Pointer> MarkAndSweep::computePos(const RefMap& refs) const {
		PosMap pos;
		// collect alive objects
		for (const auto& r : refs)
			pos.push_back(std::make_pair(r.first, NULL_PTR));
		// sort by address
		std::sort(pos.begin(), pos.end());

		// compute new positions
		Pointer free = _data; align(free);
		for (auto& p : pos) {
			p.second = tagPointer(free, extractTag(p.first));
			free += refs.at(p.first).first; align(free);
		}

		return std::make_pair(std::move(pos), free);
	}

	void MarkAndSweep::updateRefs(const RefMap& refs, const PosMap& pos) const {
		for(const auto& p : pos) {
			for (auto ptr : refs.at(p.first).second) {
				*ptr = p.second;
			}
		}
	}

	void MarkAndSweep::move(const RefMap& refs, const PosMap& pos) const {
		for (const auto& p : pos) {
			std::memmove(untagPointer(p.second), untagPointer(p.first), refs.at(p.first).first);
		}
	}

	MarkAndSweep::MarkAndSweep(Pointer data, FieldCntFn fieldCnt, RootsFn roots):
		_data(data),
		_fieldCnt(std::move(fieldCnt)),
		_roots(std::move(roots)) {}

	gc_p MarkAndSweep::make(Pointer data, FieldCntFn fieldCnt, RootsFn roots) {
		return std::make_unique<MarkAndSweep>(data, std::move(fieldCnt), std::move(roots));
	}

	Pointer MarkAndSweep::collect(Pointer) const {
		auto refs = mark(_roots());
		auto pos = computePos(refs);
		updateRefs(refs, pos.first);
		move(refs, pos.first);
		return pos.second;
	}
}
#include <interpreters.d/memory/gc.d/no_gc.h>

#include <cstring>
#include <utility>

namespace interpreters {
	gc_p NoGarbageCollector::make() {
		return std::make_unique<NoGarbageCollector>();
	}

	Pointer NoGarbageCollector::collect(Pointer free) const {
		return free;
	}
}
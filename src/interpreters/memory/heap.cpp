#include <interpreters.d/memory/heap.h>

namespace interpreters {

	Heap::Heap(std::size_t size, const GCGenerator& gc, heap_reporter_p reporter):
		_data(Pointer(malloc(size))),
		_free(_data),
		_size(size),
		_gc(gc(_data)),
		_reporter(std::move(reporter)) {

		align();
		_reporter->setSizeFn([this](){
			return this->used();
		});
		_reporter->report(HeapEvent::START);
	}

	void Heap::align() {
		_free += (TAG_SIZE - (uint64_t(_free) & uint64_t(tag::MASK))) % TAG_SIZE;
	}

	void Heap::fitOrCollect(std::size_t s) {
		if (free() < s) {
			_free = _gc->collect(_free);
			_reporter->report(HeapEvent::GC);
		} // try collect
		if (free() < s) {
			throw critical_error("out of memory");
		} // fail
	}

	std::size_t Heap::capacity() const {
		return _size;
	}

	std::size_t Heap::used() const {
		return _free - _data;
	}

	std::size_t Heap::free() const {
		return _size - used();
	}

	Pointer Heap::alloc(Unit) {
		return NULL_PTR;
	}

	Pointer Heap::alloc(Integer i) {
		return tagPointer(Pointer(long(i) << 2l), tag::INTEGER);
	}

	Pointer Heap::alloc(Boolean b) {
		return tagPointer(Pointer(long(b) << 2l), tag::BOOLEAN);
	}

	Pointer Heap::alloc(Array a, Pointer init) {
		fitOrCollect(sizeof(a.size) + a.size * sizeof(Pointer));
		auto ptr = _free;
		push(a.size);
		for (std::size_t i = 0; i < std::size_t(a.size); ++i) {
			push(init);
		}
		align();
		_reporter->report(HeapEvent::ALLOC);
		return tagPointer(ptr, tag::ARRAY);
	}

	Pointer Heap::alloc(Object o, std::stack<Pointer> fields) {
		fitOrCollect(sizeof(o.proto) + sizeof(o.parent) + fields.size() * sizeof(Pointer));
		auto ptr = _free;
		push(o.proto);
		push(o.parent);
		while(!fields.empty()) {
			push(fields.top());
			fields.pop();
		}
		align();
		_reporter->report(HeapEvent::ALLOC);
		return tagPointer(ptr, tag::OBJECT);
	}

	void Heap::clear() {
		_free = _data;
		align();
	}
}
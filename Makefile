#
# Compiler
#
CXX=g++
CXX_FLAGS=-std=c++20 -Wall -Werror -Wextra -pedantic -Iinclude/

#
# Sources
#
SRC=\
	main.cpp\
	ast/nodes/access_field.cpp\
	ast/nodes/access_variable.cpp\
	ast/nodes/array.cpp\
	ast/nodes/assign_field.cpp\
	ast/nodes/assign_variable.cpp\
	ast/nodes/block.cpp\
	ast/nodes/boolean.cpp\
	ast/nodes/call_function.cpp\
	ast/nodes/call_method.cpp\
	ast/nodes/conditional.cpp\
	ast/nodes/function.cpp\
	ast/nodes/identifier.cpp\
	ast/nodes/integer.cpp\
	ast/nodes/loop.cpp\
	ast/nodes/null.cpp\
	ast/nodes/object.cpp\
	ast/nodes/print.cpp\
	ast/nodes/top.cpp\
	ast/nodes/variable.cpp\
	ast/converters/json.cpp\
	bytecode/constant_pool.cpp\
	bytecode/program.cpp\
	compiler/compiler.cpp\
	compiler/label_generator.cpp\
	interpreters/interpreter.cpp\
	interpreters/memory/heap.cpp\
	interpreters/memory/gc/mark_and_sweep.cpp\
	interpreters/memory/gc/no_gc.cpp\
	interpreters/memory/reporter/csv_reporter.cpp\
	interpreters/memory/reporter/heap_reporter.cpp\
	interpreters/memory/reporter/void_reporter.cpp
OBJ=$(SRC:.cpp=.o)
EXE=fml

#
# Common
#
BUILD_DIR=build.d
ifdef AVAILABLE_MEMORY
PASS_VARS=-DAVAILABLE_MEMORY=${AVAILABLE_MEMORY}
endif

#
# Debug
#
DEBUG_DIR=${BUILD_DIR}/debug
DEBUG_EXE=${DEBUG_DIR}/${EXE}
DEBUG_OBJ=$(addprefix ${DEBUG_DIR}/, ${OBJ})
DEBUG_FLAGS=-g -O0 -DDEBUG ${PASS_VARS}

#
# Release
#
RELEASE_DIR=${BUILD_DIR}/release
RELEASE_EXE=${RELEASE_DIR}/${EXE}
RELEASE_OBJ=$(addprefix ${RELEASE_DIR}/, ${OBJ})
RELEASE_FLAGS=-O3 -DNDEBUG ${PASS_VARS}

.PHONY: all debug release clean rebuild prepare

all: prepare release

#
# Debug
#
debug: ${DEBUG_EXE}

${DEBUG_EXE}: ${DEBUG_OBJ}
	${CXX} ${CXX_FLAGS} ${DEBUG_FLAGS} -o $@ $^

${DEBUG_DIR}/%.o: src/%.cpp
	${CXX} -c ${CXX_FLAGS} ${DEBUG_FLAGS} -o $@ $<

#
# Release
#
release: ${RELEASE_EXE}

${RELEASE_EXE}: ${RELEASE_OBJ}
	${CXX} ${CXX_FLAGS} ${RELEASE_FLAGS} -o $@ $^

${RELEASE_DIR}/%.o: src/%.cpp
	${CXX} -c ${CXX_FLAGS} ${RELEASE_FLAGS} -o $@ $<

#
# Other
#
clean:
	rm -f ${DEBUG_EXE} ${DEBUG_OBJ} ${RELEASE_EXE} ${RELEASE_OBJ}

rebuild: clean all

prepare:
	@mkdir -p ${DEBUG_DIR} ${RELEASE_DIR}
	@mkdir -p ${DEBUG_DIR}/ast ${RELEASE_DIR}/ast
	@mkdir -p ${DEBUG_DIR}/ast/converters ${RELEASE_DIR}/ast/converters
	@mkdir -p ${DEBUG_DIR}/ast/nodes ${RELEASE_DIR}/ast/nodes
	@mkdir -p ${DEBUG_DIR}/bytecode ${RELEASE_DIR}/bytecode
	@mkdir -p ${DEBUG_DIR}/compiler ${RELEASE_DIR}/compiler
	@mkdir -p ${DEBUG_DIR}/interpreters ${RELEASE_DIR}/interpreters
	@mkdir -p ${DEBUG_DIR}/interpreters/memory ${RELEASE_DIR}/interpreters/memory
	@mkdir -p ${DEBUG_DIR}/interpreters/memory/gc ${RELEASE_DIR}/interpreters/memory/gc
	@mkdir -p ${DEBUG_DIR}/interpreters/memory/reporter ${RELEASE_DIR}/interpreters/memory/reporter
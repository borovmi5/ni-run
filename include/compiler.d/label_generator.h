#pragma once

#include <string>

namespace compiler::labels {
	using label = std::string;
	struct ConditionalLabels { label consequent; label end; };
	struct LoopLabels { label body; label condition; };

	ConditionalLabels conditional();
	LoopLabels loop();
}
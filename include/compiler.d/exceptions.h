#pragma once

#include <stdexcept>
#include <sstream>
#include <string>

namespace compiler {
	class semantic_exception : public std::runtime_error {
			std::string _msg;

		public:
			template <typename... Ts>
			explicit semantic_exception(Ts... args) : std::runtime_error("") {
				std::ostringstream oss;
				((oss << args), ...);
				_msg = oss.str();
			}

			const char * what() const noexcept override {
				return _msg.data();
			}
	};
}
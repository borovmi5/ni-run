#include "../bytecode"

#include <map>

namespace compiler {
	struct POCmp {
		inline bool operator()(const bc::ProgramObject& lhs, const bc::ProgramObject& rhs) const {
			if (lhs.index() != rhs.index())
				return lhs.index() < rhs.index();
			switch (bc::type(lhs.index())) {
				case bc::type::INTEGER:
					return std::get<bc::Integer>(lhs) < std::get<bc::Integer>(rhs);
				case bc::type::UNIT:
					return false;
				case bc::type::STRING:
					return std::get<bc::String>(lhs) < std::get<bc::String>(rhs);
				case bc::type::METHOD: {
					auto lm = std::get<bc::Method>(lhs);
					auto rm = std::get<bc::Method>(rhs);
					return std::make_tuple(lm.name, lm.args, lm.start, lm.locals, lm.len) <
					       std::make_tuple(rm.name, rm.args, rm.start, rm.locals, rm.len);
				}
				case bc::type::SLOT:
					return std::get<bc::Slot>(lhs) < std::get<bc::Slot>(rhs);
				case bc::type::CLASS:
					return std::get<bc::Class>(lhs) < std::get<bc::Class>(rhs);
				case bc::type::BOOLEAN:
					return std::get<bc::Boolean>(lhs) < std::get<bc::Boolean>(rhs);
				default:
					throw std::runtime_error("should never happen");
			}
		}
	};

	using ConstantMap = std::map<bc::ProgramObject, bc::cpIndex, POCmp>;
}
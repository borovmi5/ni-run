#pragma once

#include "constant_map.h"
#include "exceptions.h"

#include "../ast"
#include "../bytecode"

#include <map>
#include <set>
#include <stack>

namespace compiler {
	using Globals = std::map<std::string, bc::cpIndex>;
	using Locals = std::map<std::string, bc::lfIndex>;

	enum Scope: unsigned int {
		GLOBAL     = 0, LOCAL = 1, CLASS_GLOBAL = 2, CLASS_LOCAL = 3,
		LOCAL_MASK = 1, CLASS_MASK = 2
	};
	inline bool isLocal(Scope s) { return s & LOCAL_MASK; }
	inline bool isGlobal(Scope s) { return !isLocal(s); }
	inline bool isClass(Scope s) { return s & CLASS_MASK; }
	inline bool isNotClass(Scope s) { return !isClass(s); }

	using GlobalIndices = std::vector<bc::cpIndex>;

	class Compiler : public ast::IVisitor {
			// state
			Scope _scope = GLOBAL;

			// code
			bc::Code _code;
			std::stack<bc::Code> _codeStack;

			// variables
			GlobalIndices _indices;
			Globals _globals;
			std::stack<Locals> _locals;

			// constants
			ConstantMap _constants;

		protected:
			bc::cpIndex getOrCreateConstant(const bc::ProgramObject& object);
			std::pair<uint16_t, bool> getIdent(const std::string& name);
			bc::cpIndex getOrCreateGlobal(const std::string& g);
			bc::lfIndex createLocal(const std::string& l);
			void pushInst(bc::OpCode&& op);
			void popInst();

		public:
			std::any visit(ast::AccessField& node) override;
			std::any visit(ast::AccessVariable& node) override;
			std::any visit(ast::Array& node) override;
			std::any visit(ast::AssignField& node) override;
			std::any visit(ast::AssignVariable& node) override;
			std::any visit(ast::Block& node) override;
			std::any visit(ast::Boolean& node) override;
			std::any visit(ast::CallFunction& node) override;
			std::any visit(ast::CallMethod& node) override;
			std::any visit(ast::Conditional& node) override;
			std::any visit(ast::Function& node) override;
			std::any visit(ast::Identifier&) override;
			std::any visit(ast::Integer& node) override;
			std::any visit(ast::Loop& node) override;
			std::any visit(ast::Null& node) override;
			std::any visit(ast::Object& node) override;
			std::any visit(ast::Print& node) override;
			std::any visit(ast::Top& node) override;
			std::any visit(ast::Variable& node) override;
	};
}
#pragma once

#include "constant_pool.h"
#include "opcode.h"
#include "types.h"

#include <iostream>
#include <vector>

namespace bc {
	using Globals = std::vector<cpIndex>;
	using Code    = std::vector<OpCode>;

	class Program {
			ConstantPool _constPool;
			Globals      _globals;
			cpIndex      _entryPoint = 0;
			Code         _code;

		public:
			Program() = default;
			Program(ConstantPool&& pool, Globals&& globals, cpIndex ep, Code&& code);

			[[nodiscard]] const ProgramObject& getConst(cpIndex id) const;
			[[nodiscard]] std::size_t constCount() const;
			[[nodiscard]] const ProgramObject& getGlobal(cpIndex id) const;
			[[nodiscard]] cpIndex getGlobalIndex(cpIndex id) const;
			[[nodiscard]] std::size_t globalCount() const;
			[[nodiscard]] codeAddr getEntryPoint() const;
			[[nodiscard]] arity getArity() const;
			[[nodiscard]] size getLocals() const;
			[[nodiscard]] const OpCode& getCode(codeAddr addr) const;
			[[nodiscard]] codeAddr getEndPoint() const;

			static Program fromStream(std::istream& stream);
			std::ostream& toStream(std::ostream& stream) const;
	};
}
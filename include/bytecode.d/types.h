#pragma once

#include <cstdint>
#include <cstddef>
#include <string>
#include <variant>
#include <vector>

namespace bc {
	using byte = uint8_t;

	using cpIndex  = uint16_t;
	using lfIndex  = uint16_t;
	using arity    = uint8_t;
	using size     = uint16_t;
	using length   = uint32_t;
	using codeAddr = uint64_t;

	using Integer = int32_t;
	struct Null {};
	const Null Null;
	using String  = std::string;
	struct Method {
		cpIndex  name;
		arity    args;
		size     locals;
		length   len;
		codeAddr start;
	};
	using Slot    = cpIndex;
	using Class   = std::vector<cpIndex>;
	using Boolean = bool;

	using ProgramObject = std::variant<Integer, struct Null, String, Method, Slot, Class, Boolean>;

	enum class type : byte {
		INTEGER   = 0x00, // integer
		UNIT      = 0x01, // unit
		STRING    = 0x02, // string
		METHOD    = 0x03, // method
		SLOT      = 0x04, // slot
		CLASS     = 0x05, // class
		BOOLEAN   = 0x06  // boolean
	};
}
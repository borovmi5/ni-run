#pragma once

#include "types.h"

#include <iostream>
#include <vector>

namespace bc {
	class ConstantPool {
			std::vector<ProgramObject> _data;

		public:
			ConstantPool() = default;
			explicit ConstantPool(std::vector<ProgramObject>&& data);

			[[nodiscard]] const ProgramObject& get(cpIndex id) const;
			[[nodiscard]] std::size_t size() const;
			void push(ProgramObject object);
	};
}
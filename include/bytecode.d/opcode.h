#pragma once

#include "types.h"

#include <cstddef>
#include <variant>

namespace bc {
	struct Label { cpIndex name; };
	struct Literal { cpIndex value; };
	struct Print { cpIndex fmt; arity args; };
	struct Array {};
	struct Object { cpIndex proto; };
	struct GetField { cpIndex name; };
	struct SetField { cpIndex name; };
	struct CallMethod { cpIndex name; arity args; };
	struct CallFunction { cpIndex name; arity args; };
	struct SetLocal { lfIndex id; };
	struct GetLocal { lfIndex id; };
	struct SetGlobal { cpIndex name; };
	struct GetGlobal { cpIndex name; };
	struct Branch { cpIndex label; };
	struct Jump { cpIndex label; };
	struct Return {};
	struct Drop {};

	using OpCode = std::variant<
		Label, Literal, Print, struct Array,
		Object, GetField, SetField, CallMethod,
		CallFunction, SetLocal, GetLocal, SetGlobal,
		GetGlobal, Branch, Jump, struct Return, struct Drop>;

	enum class opcode : uint8_t {
		LABEL         = 0x00, // | OPCODE [1B] | CP_IDX [2B] |
		LITERAL       = 0x01, // | OPCODE [1B] | CP_IDX [2B] |
		PRINT         = 0x02, // | OPCODE [1B] | CP_IDX [2B] | ARG_CNT [1B] |
		ARRAY         = 0x03, // | OPCODE [1B] |
		OBJECT        = 0x04, // | OPCODE [1B] | CP_IDX [2B] |
		GET_FIELD     = 0x05, // | OPCODE [1B] | CP_IDX [2B] |
		SET_FIELD     = 0x06, // | OPCODE [1B] | CP_IDX [2B] |
		CALL_METHOD   = 0x07, // | OPCODE [1B] | CP_IDX [2B] | ARG_CNT [1B] |
		CALL_FUNCTION = 0x08, // | OPCODE [1B] | CP_IDX [2B] | ARG_CNT [1B] |
		SET_LOCAL     = 0x09, // | OPCODE [1B] | LF_IDX [2B] |
		GET_LOCAL     = 0x0a, // | OPCODE [1B] | LF_IDX [2B] |
		SET_GLOBAL    = 0x0b, // | OPCODE [1B] | CP_IDX [2B] |
		GET_GLOBAL    = 0x0c, // | OPCODE [1B] | CP_IDX [2B] |
		BRANCH        = 0x0d, // | OPCODE [1B] | CP_IDX [2B] |
		JUMP          = 0x0e, // | OPCODE [1B] | CP_IDX [2B] |
		RETURN        = 0x0f, // | OPCODE [1B] |
		DROP          = 0x10  // | OPCODE [1B] |
	};

	inline constexpr std::size_t opsize(opcode op) {
		switch (op) {
			case opcode::ARRAY:
			case opcode::RETURN:
			case opcode::DROP:
				return sizeof(opcode);
			case opcode::LABEL:
			case opcode::LITERAL:
			case opcode::OBJECT:
			case opcode::GET_FIELD:
			case opcode::SET_FIELD:
			case opcode::SET_GLOBAL:
			case opcode::GET_GLOBAL:
			case opcode::BRANCH:
			case opcode::JUMP:
				return sizeof(opcode) + sizeof(cpIndex);
			case opcode::SET_LOCAL:
			case opcode::GET_LOCAL:
				return sizeof(opcode) + sizeof(lfIndex);
			case opcode::PRINT:
			case opcode::CALL_METHOD:
			case opcode::CALL_FUNCTION:
				return sizeof(opcode) + sizeof(cpIndex) + sizeof(arity);
		}
	}
}
#pragma once

#include "exceptions.h"
#include "types.h"
#include "memory/frame_stack.h"
#include "memory/heap.h"
#include "memory/operand_stack.h"
#include "memory/gc"
#include "memory/reporter"

#include "../bytecode"

#include <concepts>
#include <stack>
#include <unordered_map>
#include <vector>

namespace interpreters {
	using LabelTable = std::unordered_map<bc::String, bc::codeAddr>;
	using Functions = std::unordered_map<bc::String, bc::cpIndex>;

	enum class GCType {
			NO_GC, MARK_AND_SWEEP
	};

	class BCInterpreter {
			bc::Program _program;
			bc::codeAddr _pc = 0;

			Heap         _heap;
			OperandStack _stack;
			GlobalFrame  _globalFrame;
			FrameStack   _frameStack;
			Functions    _functions;
			LabelTable   _labelTable;

			static std::string print(const bc::ProgramObject& o);
			std::string print(const RuntimeObject& o);

		public:
			explicit BCInterpreter(std::size_t heapSize, GCType gcType = GCType::NO_GC, heap_reporter_p reporter = VoidReporter::make());

			void setProgram(bc::Program program);
			void setProgram(std::ifstream& source);

			void reset();
			void step();
			void run();

			void operator()(bc::Program program);
			void operator()(std::ifstream& source);

		protected:
			void process(bc::Label);
			void process(bc::Literal op);
			void process(bc::Print op);
			void process(bc::Array);
			void process(bc::Object op);
			void process(bc::GetField op);
			void process(bc::SetField op);
			void process(bc::CallMethod op);
			void process(bc::CallFunction op);
			void process(bc::SetLocal op);
			void process(bc::GetLocal op);
			void process(bc::SetGlobal op);
			void process(bc::GetGlobal op);
			void process(bc::Branch op);
			void process(bc::Jump op);
			void process(bc::Return op);
			void process(bc::Drop);
	};
}
#pragma once

#include <cstdlib>
#include <stack>

#include "pointer_tagging.h"

#include "reporter.d/heap_reporter.h"
#include "gc.d/gc.h"

namespace interpreters {
	class Heap {
		public:
			using GCGenerator = std::function<gc_p(Pointer)>;

		protected:
			Pointer _data;
			Pointer _free;
			std::size_t _size;

			gc_p _gc;
			heap_reporter_p _reporter;

			void align();
			template <typename T>
			void push(T val) {
				*(T*)_free = val;
				_free += sizeof(val);
			}

			void fitOrCollect(std::size_t s);

		public:
			Heap(std::size_t size, const GCGenerator& gc, heap_reporter_p reporter);
			~Heap() { std::free(_data); }

			std::size_t capacity() const;
			std::size_t used() const;
			std::size_t free() const;

			Pointer alloc(Unit);
			Pointer alloc(Integer i);
			Pointer alloc(Boolean b);
			Pointer alloc(Array a, Pointer init);
			Pointer alloc(Object o, std::stack<Pointer> fields);

			void clear();
	};
}
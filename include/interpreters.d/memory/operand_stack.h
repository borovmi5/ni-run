#pragma once

#include "../exceptions.h"
#include "../types.h"

#include <stack>

namespace interpreters {
	class OperandStack {
			std::deque<Pointer> _data;

		public:
			inline bool empty(std::stack<Pointer>::size_type cnt = 1) const { return _data.size() < cnt; }
			inline void test(std::stack<Pointer>::size_type cnt = 1) const {
				if (empty(cnt)) throw critical_error("operand stack empty");
			}

			inline void push(Pointer p) { _data.push_back(p); }

			inline Pointer peek() const { test(); return _data.back(); }
			inline Pointer peekPtr() const { return peek(); }
			inline RuntimeObject peekRO() const { return deref(peek()); }

			inline void pop() { test(); _data.pop_back(); }
			inline Pointer popPtr() { auto p = peekPtr(); pop(); return p; }
			inline RuntimeObject popRO() { auto o = peekRO(); pop(); return o; }

			inline auto begin() { return _data.begin(); }
			inline auto end() { return _data.end(); }
	};
}
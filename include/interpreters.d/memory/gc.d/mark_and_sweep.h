#pragma once

#include "gc.h"
#include "../../types.h"

#include <iostream>
#include <functional>
#include <queue>
#include <map>
#include <vector>

namespace interpreters {
	class MarkAndSweep: public GarbageCollector {
		public:
			using Queue  = std::queue<Pointer*>;
			using FieldCntFn = std::function<int(const Object&)>;
			using RootsFn = std::function<Queue()>;
		protected:
			using RefMap = std::map<Pointer, std::pair<std::size_t, std::vector<Pointer*>>>;
			using PosMap = std::vector<std::pair<Pointer, Pointer>>;

			Pointer _data;

			FieldCntFn _fieldCnt;
			RootsFn _roots;

			RefMap mark(Queue q) const;
			std::pair<PosMap, Pointer> computePos(const RefMap& refs) const;
			void updateRefs(const RefMap& refs, const PosMap& pos) const;
			void move(const RefMap& refs, const PosMap& pos) const;

		public:
			MarkAndSweep(Pointer data, FieldCntFn fieldCnt, RootsFn roots);

			static gc_p make(Pointer data, FieldCntFn fieldCnt, RootsFn roots);

			Pointer collect(Pointer free) const override;
	};
}
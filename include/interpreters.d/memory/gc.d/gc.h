#pragma once

#include "../pointer_tagging.h"

#include <memory>

namespace interpreters {
	class GarbageCollector {
		public:
			virtual Pointer collect(Pointer free) const = 0;
	};

	using gc_p = std::unique_ptr<GarbageCollector>;
}
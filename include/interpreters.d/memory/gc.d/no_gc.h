#pragma once

#include "gc.h"

namespace interpreters {
	class NoGarbageCollector: public GarbageCollector {
		public:
			static gc_p make();

			Pointer collect(Pointer free) const override;
	};
}
#pragma once

#include <cstdlib>

#include "../exceptions.h"
#include "../types.h"

namespace interpreters {
	const constexpr Pointer NULL_PTR = nullptr;

	enum class tag : bc::byte {
		// NULL is special address
		OBJECT  = 0x00, // 00
		ARRAY   = 0x01, // 01
		INTEGER = 0x02, // 10
		BOOLEAN = 0x03, // 11
		MASK = INTEGER | BOOLEAN | ARRAY | OBJECT
	};
	const constexpr std::size_t TAG_SIZE = 0x4;

	inline Pointer tagPointer  (Pointer p, tag t) { return Pointer(uint64_t(p) | uint64_t(t));          }
	inline Pointer untagPointer(Pointer p)        { return Pointer(uint64_t(p) & ~uint64_t(tag::MASK)); }
	inline tag     extractTag  (Pointer p)        { return tag(uint64_t(p)     & uint64_t(tag::MASK));  }

	inline RuntimeObject deref(Pointer p) {
		if (p == NULL_PTR)
			return Null;
		switch (extractTag(p)) {
			case tag::INTEGER:
				return Integer(long(untagPointer(p)) >> 2l);
			case tag::BOOLEAN:
				return Boolean(long(untagPointer(p)) >> 2l);
			case tag::ARRAY: {
				auto up = untagPointer(p);
				return Array{
					*(Integer*)up,
					(Pointer*)(up + sizeof(Integer))
				};
			}
			case tag::OBJECT: {
				auto up = untagPointer(p);
				return Object {
					*(bc::cpIndex*)up,
					*(Pointer*)(up + sizeof(bc::cpIndex)),
					(Pointer*)(up + sizeof(bc::cpIndex) + sizeof(Pointer))
				};
			}
			default:
				throw critical_error("should never happen");
		}
	}
}
#pragma once

#include "../types.h"

#include <deque>
#include <unordered_map>

namespace interpreters {
	struct Frame {
		std::vector<Pointer> values;
		bc::codeAddr ra;
	};
	using FrameStack = std::deque<Frame>;
	using GlobalFrame = std::unordered_map<bc::String, Pointer>;
}
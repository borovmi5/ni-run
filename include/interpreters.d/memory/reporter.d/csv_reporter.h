#pragma once

#include "heap_reporter.h"

#include <iostream>

namespace interpreters {
	class CsvReporter: public HeapReporter {
		protected:
			std::ostream& _out;

		public:
			CsvReporter(std::ostream& out);

			static heap_reporter_p make(std::ostream& out);

		protected:
			void report(HeapEvent e) const override;
	};
}
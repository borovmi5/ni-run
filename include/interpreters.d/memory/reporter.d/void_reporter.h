#pragma once

#include "heap_reporter.h"

namespace interpreters {
	class VoidReporter: public HeapReporter {
		public:
			static heap_reporter_p make();

		protected:
			void report(HeapEvent e) const override;
	};
}
#pragma once

#include <cstddef>
#include <functional>
#include <memory>

namespace interpreters {
	enum class HeapEvent {
		START, ALLOC, GC
	};

	inline const char* toString(HeapEvent e) {
		switch (e) {
			case HeapEvent::START: return "S";
			case HeapEvent::ALLOC: return "A";
			case HeapEvent::GC: return "G";
		}
		return "";
	}

	class HeapReporter {
		public:
			using SizeFn = std::function<std::size_t()>;

		protected:
			SizeFn _size;
			void setSizeFn(SizeFn size);

			virtual void report(HeapEvent e) const = 0;

			friend class Heap;
	};

	using heap_reporter_p = std::unique_ptr<HeapReporter>;
}
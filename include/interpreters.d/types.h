#pragma once

#include "../bytecode.d/types.h"

#include <cstddef>
#include <cstdint>
#include <optional>
#include <variant>

namespace interpreters {
	using Pointer = bc::byte *;

	struct Unit {};
	inline const constexpr Unit Null;
	using Integer = int32_t;
	using Boolean = bool;
	struct Array {
		Integer  size;
		Pointer* content;
	};
	struct Object {
		bc::cpIndex proto;
		Pointer parent;
		Pointer* content;
	};

	using RuntimeObject = std::variant<Unit, Integer, Boolean, Array, Object>;

	enum class type : bc::byte {
			UNIT = 0x00,
			INTEGER = 0x01,
			BOOLEAN = 0x02,
			ARRAY = 0x03,
			OBJECT = 0x04,
	};
}
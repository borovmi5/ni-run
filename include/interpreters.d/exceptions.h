#pragma once

#include <stdexcept>
#include <sstream>
#include <string>

namespace interpreters {
	class interpret_exception : public std::runtime_error {
			std::string _msg;

		public:
			template <typename... Ts>
			explicit interpret_exception(Ts... args) : std::runtime_error("") {
				std::ostringstream oss;
				((oss << args), ...);
				_msg = oss.str();
			}

			const char * what() const noexcept override {
				return _msg.data();
			}
	};

	class critical_error : public std::runtime_error {
			std::string _msg;

		public:
			template <typename... Ts>
			explicit critical_error(Ts... args) : std::runtime_error("") {
				std::ostringstream oss;
				((oss << args), ...);
				_msg = oss.str();
			}

			const char * what() const noexcept override {
				return _msg.data();
			}
	};
}
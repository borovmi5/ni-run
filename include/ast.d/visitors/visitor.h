#pragma once

#include "../../ast"

#include <any>

namespace ast {
	class IVisitor {
		public:
			virtual std::any visit(AccessField& node) = 0;
			virtual std::any visit(AccessVariable& node) = 0;
			virtual std::any visit(Array& node) = 0;
			virtual std::any visit(AssignField& node) = 0;
			virtual std::any visit(AssignVariable& node) = 0;
			virtual std::any visit(Block& node) = 0;
			virtual std::any visit(Boolean& node) = 0;
			virtual std::any visit(CallFunction& node) = 0;
			virtual std::any visit(CallMethod& node) = 0;
			virtual std::any visit(Conditional& node) = 0;
			virtual std::any visit(Function& node) = 0;
			virtual std::any visit(Identifier& node) = 0;
			virtual std::any visit(Integer& node) = 0;
			virtual std::any visit(Loop& node) = 0;
			virtual std::any visit(Null& node) = 0;
			virtual std::any visit(Object& node) = 0;
			virtual std::any visit(Print& node) = 0;
			virtual std::any visit(Top& node) = 0;
			virtual std::any visit(Variable& node) = 0;
	};

	inline std::any AccessField::accept(IVisitor& visitor) { return visitor.visit(*this); }
	inline std::any AccessVariable::accept(IVisitor& visitor) { return visitor.visit(*this); }
	inline std::any Array::accept(IVisitor& visitor) { return visitor.visit(*this); }
	inline std::any AssignField::accept(IVisitor& visitor) { return visitor.visit(*this); }
	inline std::any AssignVariable::accept(IVisitor& visitor) { return visitor.visit(*this); }
	inline std::any Block::accept(IVisitor& visitor) { return visitor.visit(*this); }
	inline std::any Boolean::accept(IVisitor& visitor) { return visitor.visit(*this); }
	inline std::any CallFunction::accept(IVisitor& visitor) { return visitor.visit(*this); }
	inline std::any CallMethod::accept(IVisitor& visitor) { return visitor.visit(*this); }
	inline std::any Conditional::accept(IVisitor& visitor) { return visitor.visit(*this); }
	inline std::any Function::accept(IVisitor& visitor) { return visitor.visit(*this); }
	inline std::any Identifier::accept(IVisitor& visitor) { return visitor.visit(*this); }
	inline std::any Integer::accept(IVisitor& visitor) { return visitor.visit(*this); }
	inline std::any Loop::accept(IVisitor& visitor) { return visitor.visit(*this); }
	inline std::any Null::accept(IVisitor& visitor) { return visitor.visit(*this); }
	inline std::any Object::accept(IVisitor& visitor) { return visitor.visit(*this); }
	inline std::any Print::accept(IVisitor& visitor) { return visitor.visit(*this); }
	inline std::any Top::accept(IVisitor& visitor) { return visitor.visit(*this); }
	inline std::any Variable::accept(IVisitor& visitor) { return visitor.visit(*this); }
}
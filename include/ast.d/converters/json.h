#pragma once

#include "../../ast"

#include <iostream>

#include "../../nlohmann/json.hpp"

namespace ast {
	ast_p fromJSON(const nlohmann::json& source);

	[[maybe_unused]] ast_p fromJSONStream(std::istream& stream);
}
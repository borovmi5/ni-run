#pragma once

#include "ast.h"
#include "identifier.h"

#include <memory>

namespace ast {
	class AccessField : public AST {
			ast_p       _object;
			identifier_p _field;

		public:
			AccessField(ast_p object, identifier_p field);

			ast_p getObject() const;
			identifier_p getField() const;

			std::any accept(IVisitor& visitor) override;
	};

	using access_field_p = std::shared_ptr<AccessField>;
}
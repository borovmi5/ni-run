#pragma once

#include "ast.h"

#include <memory>

namespace ast {
	class Boolean : public AST {
			bool _value;

		public:
			Boolean(bool value);

			bool getValue() const;

			std::any accept(IVisitor& visitor) override;
	};

	using boolean_p = std::shared_ptr<Boolean>;
}
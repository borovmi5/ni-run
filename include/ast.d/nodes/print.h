#pragma once

#include "ast.h"

#include <memory>
#include <string>
#include <vector>

namespace ast {
	class Print : public AST {
			std::string           _format;
			std::vector<ast_p> _arguments;

		public:
			Print(std::string format, std::vector<ast_p> arguments);

			const std::string& getFormat() const;
			const std::vector<ast_p>& getArguments() const;

			std::any accept(IVisitor& visitor) override;
	};

	using print_p = std::shared_ptr<Print>;
}
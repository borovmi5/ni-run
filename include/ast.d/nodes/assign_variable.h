#pragma once

#include "ast.h"
#include "identifier.h"

#include <memory>

namespace ast {
	class AssignVariable : public AST {
			identifier_p _name;
			ast_p       _value;

		public:
			AssignVariable(identifier_p name, ast_p value);

			identifier_p getName() const;
			ast_p getValue() const;

			std::any accept(IVisitor& visitor) override;
	};

	using assign_variable_p = std::shared_ptr<AssignVariable>;
}
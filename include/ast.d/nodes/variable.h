#pragma once

#include "ast.h"
#include "identifier.h"

#include <memory>

namespace ast {
	class Variable : public AST {
			identifier_p _name;
			ast_p       _value;

		public:
			Variable(identifier_p name, ast_p value);

			identifier_p getName() const;
			ast_p getValue() const;

			std::any accept(IVisitor& visitor) override;
	};

	using variable_p = std::shared_ptr<Variable>;
}
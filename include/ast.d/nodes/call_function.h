#pragma once

#include "ast.h"
#include "identifier.h"

#include <memory>
#include <vector>

namespace ast {
	class CallFunction : public AST {
			identifier_p            _name;
			std::vector<ast_p> _arguments;

		public:
			CallFunction(identifier_p name, std::vector<ast_p> arguments);

			identifier_p getName() const;
			const std::vector<ast_p>& getArguments() const;

			std::any accept(IVisitor& visitor) override;
	};

	using call_function_p = std::shared_ptr<CallFunction>;
}
#pragma once

#include "ast.h"

#include <memory>
#include <vector>

namespace ast {
	class Object : public AST {
			ast_p              _extends;
			std::vector<ast_p> _members;

		public:
			Object(ast_p extends, std::vector<ast_p> members);

			ast_p getExtends() const;
			const std::vector<ast_p>& getMembers() const;

			std::any accept(IVisitor& visitor) override;
	};

	using object_p = std::shared_ptr<Object>;
}
#pragma once

#include "ast.h"
#include "identifier.h"

#include <memory>

namespace ast {
	class AccessVariable : public AST {
			identifier_p _name;

		public:
			AccessVariable(identifier_p name);

			identifier_p getName() const;

			std::any accept(IVisitor& visitor) override;
	};

	using access_variable_p = std::shared_ptr<AccessVariable>;
}
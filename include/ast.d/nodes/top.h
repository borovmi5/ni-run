#pragma once

#include "ast.h"

#include <memory>
#include <vector>

namespace ast {
	class Top : public AST {
			std::vector<ast_p> _sequence;

		public:
			Top(std::vector<ast_p> sequence);

			const std::vector<ast_p>& getSequence() const;

			std::any accept(IVisitor& visitor) override;
	};

	using top_p = std::shared_ptr<Top>;
}
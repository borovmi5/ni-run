#pragma once

#include <any>
#include <memory>

namespace ast {
	class IVisitor;
	
	class AST {
		public:
			virtual std::any accept(IVisitor& visitor) = 0;
	};

	using ast_p = std::shared_ptr<AST>;
}
#pragma once

#include "ast.h"

#include <memory>

namespace ast {
	class Array : public AST {
			ast_p  _size;
			ast_p _value;

		public:
			Array(ast_p size, ast_p value);

			ast_p getSize() const;
			ast_p getValue() const;

			std::any accept(IVisitor& visitor) override;
	};

	using array_p = std::shared_ptr<Array>;
}
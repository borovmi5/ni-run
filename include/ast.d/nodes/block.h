#pragma once

#include "ast.h"

#include <memory>
#include <vector>

namespace ast {
	class Block : public AST {
			std::vector<ast_p> _sequence;

		public:
			Block(std::vector<ast_p> sequence);

			const std::vector<ast_p>& getSequence() const;

			std::any accept(IVisitor& visitor) override;
	};

	using block_p = std::shared_ptr<Block>;
}
#pragma once

#include "ast.h"
#include "identifier.h"

#include <memory>
#include <vector>

namespace ast {
	class Function : public AST {
			identifier_p                    _name;
			std::vector<identifier_p> _parameters;
			ast_p                           _body;

		public:
			Function(identifier_p name, std::vector<identifier_p> parameters, ast_p body);

			identifier_p getName() const;
			const std::vector<identifier_p>& getParameters() const;
			ast_p getBody() const;

			std::any accept(IVisitor& visitor) override;
	};

	using function_p = std::shared_ptr<Function>;
}
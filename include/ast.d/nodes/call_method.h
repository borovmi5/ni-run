#pragma once

#include "ast.h"
#include "identifier.h"

#include <memory>
#include <vector>

namespace ast {
	class CallMethod : public AST {
			ast_p                 _object;
			identifier_p            _name;
			std::vector<ast_p> _arguments;

		public:
			CallMethod(ast_p object, identifier_p name, std::vector<ast_p> arguments);

			ast_p getObject() const;
			identifier_p getName() const;
			const std::vector<ast_p>& getArguments() const;

			std::any accept(IVisitor& visitor) override;
	};

	using call_method_p = std::shared_ptr<CallMethod>;
}
#pragma once

#include "ast.h"

#include <memory>

namespace ast {
	class Loop : public AST {
			ast_p _condition;
			ast_p _body;

		public:
			Loop(ast_p condition, ast_p body);

			ast_p getCondition() const;
			ast_p getBody() const;

			std::any accept(IVisitor& visitor) override;
	};

	using loop_p = std::shared_ptr<Loop>;
}
#pragma once

#include "ast.h"

#include <memory>

namespace ast {
	class Integer : public AST {
			int _value;

		public:
			Integer(int value);

			int getValue() const;

			std::any accept(IVisitor& visitor) override;
	};

	using integer_p = std::shared_ptr<Integer>;
}
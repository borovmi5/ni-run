#pragma once

#include "ast.h"
#include "identifier.h"

#include <memory>

namespace ast {
	class AssignField : public AST {
			ast_p       _object;
			identifier_p _field;
			ast_p        _value;

		public:
			AssignField(ast_p object, identifier_p field, ast_p value);

			ast_p getObject() const;
			identifier_p getField() const;
			ast_p getValue() const;

			std::any accept(IVisitor& visitor) override;
	};

	using assign_field_p = std::shared_ptr<AssignField>;
}
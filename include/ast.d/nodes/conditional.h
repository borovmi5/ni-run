#pragma once

#include "ast.h"

#include <memory>

namespace ast {
	class Conditional : public AST {
			ast_p _condition;
			ast_p _consequent;
			ast_p _alternative;

		public:
			Conditional(ast_p condition, ast_p consequent, ast_p alternative);

			ast_p getCondition() const;
			ast_p getConsequent() const;
			ast_p getAlternative() const;

			std::any accept(IVisitor& visitor) override;
	};

	using conditional_p = std::shared_ptr<Conditional>;
}
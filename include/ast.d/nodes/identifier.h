#pragma once

#include "ast.h"

#include <memory>
#include <string>

namespace ast {
	class Identifier : public AST {
			std::string _name;

		public:
			Identifier(std::string name);

			const std::string& getName() const;

			std::any accept(IVisitor& visitor) override;
	};

	using identifier_p = std::shared_ptr<Identifier>;
}
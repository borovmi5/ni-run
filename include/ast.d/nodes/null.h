#pragma once

#include "ast.h"

#include <memory>

namespace ast {
	class Null : public AST {
		public:
			Null();

			std::any accept(IVisitor& visitor) override;
	};

	using null_p = std::shared_ptr<Null>;
}